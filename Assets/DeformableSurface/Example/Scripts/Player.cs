﻿using UnityEngine;

namespace DeformableSurfaceExtensions
{
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    public class Player : MonoBehaviour
    {
        public float MoveSpeed = 5;
        public float ShiftForce = 25;
        public float JumpForce = 5;

        private Rigidbody _rigidbody;

        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            Move();
            Jump();
        }

        void Move()
        {
            Vector3 direction;

            if (Camera.main != null)
            {
                Vector3 hor = Camera.main.transform.TransformDirection(Vector3.right) * Input.GetAxis("Horizontal");
                Vector3 vert = Camera.main.transform.TransformDirection(Vector3.forward) * Input.GetAxis("Vertical");
                direction = (hor + vert).normalized;
            }
            else direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

            float force = MoveSpeed;
            if (Input.GetKey(KeyCode.LeftShift))
                force += ShiftForce;

            _rigidbody.AddForce(direction * force);
        }

        void Jump()
        {
            if (!Input.GetKeyDown(KeyCode.Space))
                return;

            _rigidbody.AddForce(new Vector3(0, JumpForce, 0), ForceMode.Impulse);
        }
    }
}


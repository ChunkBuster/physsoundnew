﻿/*
This example script shows how objects may interact with
deformable surface. It is simply checks for collision with
the surface every frame and decreases objects velocity.
We also need to decrease Deform Speed of the surface cause
the object may deform it before the actual collision point will
appear inside the 'snowdrift'
*/

using UnityEngine;

namespace DeformableSurfaceExtensions
{
    [RequireComponent(typeof(Rigidbody))]
    public class SurfacePhysicsSphere : MonoBehaviour
    {
        public DeformableSurface SourceSurface;

        [Range(0f, 1f)]
        public float DampenFactor = 0.5f;

        private Rigidbody _rigidbody;

        void Start()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        void Update()
        {
            if (SourceSurface == null)
                return;

            //Getting the actual offset value from surface
            float offsetValue = SourceSurface.GetCollisionOffset(GetTargetPoint());
            float dampen = (offsetValue / SourceSurface.Data.MaxOffsetValue) * DampenFactor;
            
            //Now, if point has collision with the surface offset we simply reduce the velocity
            _rigidbody.velocity = Vector3.Lerp(_rigidbody.velocity, Vector3.zero, Mathf.Clamp01(dampen));
        }

        public Vector3 GetTargetPoint()
        {
            //We want to check the position UNDER our sphere
            return _rigidbody.position - new Vector3(0, transform.localScale.y * 0.45f, 0);
        }
    }
}


﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Deformable Surface/Depth Texture"
{
    Properties
    {
        _MainTex ("Base (RGB)", 2D) = "white" {}
    }
    SubShader
    {
		Pass
        {
			Lighting Off Fog { Mode Off }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
             
            uniform sampler2D _MainTex;
            uniform sampler2D _CameraDepthTexture;
            uniform half4 _MainTex_TexelSize;
 
            struct input
            {
                float4 pos : POSITION;
                half2 uv : TEXCOORD0;
            };
 
            struct output
            {
                float4 pos : SV_POSITION;
                half2 uv : TEXCOORD0;
            }; 
 
            output vert(input i)
            {
                output o;
                o.pos = UnityObjectToClipPos(i.pos);
                o.uv = MultiplyUV(UNITY_MATRIX_TEXTURE0, i.uv);

                return o;
            }
             
            fixed4 frag(output o) : COLOR
            {
#if UNITY_VERSION < 550
                return UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, o.uv));
#else
				return 1 - UNITY_SAMPLE_DEPTH(tex2D(_CameraDepthTexture, o.uv));
#endif
            }            
            ENDCG
        }
    } 
}
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Deformable Surface/Surface Editor Brush" 
{
    Properties 
	{
        _brush ("brush", 2D) = "white" {}
        [NoScaleOffset] _height_map ("height_map", 2D) = "black" {}
        [NoScaleOffset] _offset_map ("offset_map", 2D) = "black" {}
        _tess_distance ("tess_distance", Float ) = 60
        _tess_amount ("tess_amount", Float ) = 3
        _brush_hardness ("brush_hardness", Float ) = 1
        [HideInInspector] _Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader 
		{
        Tags 
		{
            "IgnoreProjector" = "True"
            "Queue" = "Transparent"
            "RenderType" = "Transparent"
        }
        Pass 
		{
            Name "FORWARD"
            Tags { "LightMode" = "ForwardBase" }
            Blend SrcAlpha OneMinusSrcAlpha
            ZWrite Off
            
            CGPROGRAM
            #pragma hull hull
            #pragma domain domain
            #pragma vertex tessvert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "Tessellation.cginc"
            #pragma multi_compile_fwdbase
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 5.0
            uniform float _tess_distance;
            uniform float _tess_amount;
            uniform sampler2D _height_map;
            uniform sampler2D _offset_map;
            uniform sampler2D _brush; uniform float4 _brush_ST;
            uniform float _brush_hardness;

            struct VertexInput 
			{
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord1 : TEXCOORD1;
            };

            struct VertexOutput 
			{
                float4 pos : SV_POSITION;
                float2 uv1 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
            };

            VertexOutput vert (VertexInput v)
			{
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }

            #ifdef UNITY_CAN_COMPILE_TESSELLATION
                struct TessVertex
				{
                    float4 vertex : INTERNALTESSPOS;
                    float3 normal : NORMAL;
                    float4 tangent : TANGENT;
                    float2 texcoord1 : TEXCOORD1;
                };

                struct OutputPatchConstant 
				{
                    float edge[3]         : SV_TessFactor;
                    float inside          : SV_InsideTessFactor;
                    float3 vTangent[4]    : TANGENT;
                    float2 vUV[4]         : TEXCOORD;
                    float3 vTanUCorner[4] : TANUCORNER;
                    float3 vTanVCorner[4] : TANVCORNER;
                    float4 vCWts          : TANWEIGHTS;
                };

                TessVertex tessvert (VertexInput v) 
				{
                    TessVertex o;
                    o.vertex = v.vertex;
                    o.normal = v.normal;
                    o.tangent = v.tangent;
                    o.texcoord1 = v.texcoord1;
                    return o;
                }

				void displacement(inout VertexInput v)
				{
					float4 hm = tex2Dlod(_height_map, float4(v.texcoord1, 0, 0));
					float4 om = tex2Dlod(_offset_map, float4(v.texcoord1, 0, 0));
					v.vertex.y += hm.r + om.r;
				}

				float Tessellation(TessVertex v)
				{
					return round(clamp(((1.0 / distance(mul(unity_ObjectToWorld, v.vertex).rgb, _WorldSpaceCameraPos)) * _tess_distance), 1.0, _tess_amount));
				}

				float4 Tessellation(TessVertex v, TessVertex v1, TessVertex v2)
				{
					float3 t = float3(Tessellation(v), Tessellation(v1), Tessellation(v2));
					return float4(t.y + t.z, t.x + t.z, t.x + t.y, t.x + t.y + t.z) / float4(2, 2, 2, 3);
				}

                OutputPatchConstant hullconst (InputPatch<TessVertex,3> v)
				{
                    OutputPatchConstant o = (OutputPatchConstant)0;
                    float4 ts = Tessellation( v[0], v[1], v[2] );
                    o.edge[0] = ts.x;
                    o.edge[1] = ts.y;
                    o.edge[2] = ts.z;
                    o.inside = ts.w;
                    return o;
                }

                [domain("tri")]
                [partitioning("fractional_odd")]
                [outputtopology("triangle_cw")]
                [patchconstantfunc("hullconst")]
                [outputcontrolpoints(3)]
                TessVertex hull (InputPatch<TessVertex,3> v, uint id : SV_OutputControlPointID) 
				{
                    return v[id];
                }

                [domain("tri")]
                VertexOutput domain (OutputPatchConstant tessFactors, const OutputPatch<TessVertex,3> vi, float3 bary : SV_DomainLocation) 
				{
                    VertexInput v = (VertexInput)0;
                    v.vertex = vi[0].vertex * bary.x + vi[1].vertex * bary.y + vi[2].vertex * bary.z;
                    v.normal = vi[0].normal * bary.x + vi[1].normal * bary.y + vi[2].normal * bary.z;
                    v.tangent = vi[0].tangent * bary.x + vi[1].tangent * bary.y + vi[2].tangent * bary.z;
                    v.texcoord1 = vi[0].texcoord1 * bary.x + vi[1].texcoord1 * bary.y + vi[2].texcoord1 * bary.z;
                    displacement(v);
                    VertexOutput o = vert(v);
                    return o;
                }
            #endif
            float4 frag(VertexOutput i) : COLOR 
			{                
                float bCol = tex2D(_brush,TRANSFORM_TEX(i.uv1, _brush)).r;               

                return fixed4(float3(0, ceil(bCol), 0), (bCol * (_brush_hardness * 0.4 + 0.4)));
            }
            ENDCG
        }        
    }
    FallBack "Diffuse"
}

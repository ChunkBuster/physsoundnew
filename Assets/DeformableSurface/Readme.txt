Deformable Surface v1.1.1

---

DX11 only!

This asset allows you to create and manage the system with realtime volumetric surface deformation. You might already noticed similar feature in some last AAA titles. Such feature will definitely make your game more dynamic and realistic looking! To achieve the best performance it uses true power of cumpute shaders along with DX11 hardware tessellation.

Read the documentation for more info

---

Changelog:

> v1.1.1
* Fixed issue with SetTexture failed and normal map generation for newly created surface

> v1.1
* Metallic Smoothness texture slot was removed. Alpha value of Albedo texture map is now used as a source for smoothness
* Inverted version of depth camera shader for Unity 5.5 was removed. Shader directive is now handle this
* Added maps quality setting which allows to save some VRAM almost without quality loss
* Editor side compute shader logic moved to separate .compute file

> v1.0
* Initial release

---

E-mail me at Voodoo2211@gmail.com if you have any questions or suggestion

Also, check out more useful things for Unity here: 
https://www.assetstore.unity3d.com/#/publisher/979
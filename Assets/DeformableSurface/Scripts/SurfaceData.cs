﻿using UnityEngine;

namespace DeformableSurfaceExtensions
{
    public class SurfaceData : ScriptableObject
    {
        #region Const
        public const int MIN_MESH_SIZE = 8;
        public const int MAX_MESH_SIZE = 248;
        public const float MIN_MESH_SCALE = 0.05f;
        public const int MAX_HEIGHTMAP_RESOLUTION = 16;
        public const int MAX_TESS_QUALITY = 9;
        public const int MAX_PERLIN_SEED = 9999;
        public const int MAX_SMOOTH_PASSES = 10;

        public static readonly string[] SHADER_TEX_NAMES =
        {
            "_MainTex", "_albedo_flat", "_BumpMap", "_normal_flat"
        };

        public static readonly string[] SHADER_MAPS_TEX_NAMES =
        {
            "_height_map", "_offset_map", "_normal_map"
        };
        #endregion
        
        #region Geometry
        public int MeshWidth
        {
            get { return _meshWidth; }
            set
            {
                if (value == _meshWidth)
                    return;

                value -= value % MIN_MESH_SIZE;
                _meshWidth = Mathf.Clamp(value, MIN_MESH_SIZE, MAX_MESH_SIZE);
                BuildMeshBase();
                BuildAllMaps();
            }
        }
        [SerializeField]
        private int _meshWidth = MIN_MESH_SIZE * 2;

        public int MeshLength
        {
            get { return _meshLength; }
            set
            {
                if (value == _meshLength)
                    return;

                value -= value % MIN_MESH_SIZE;
                _meshLength = Mathf.Clamp(value, MIN_MESH_SIZE, MAX_MESH_SIZE);

                BuildMeshBase();
                BuildAllMaps();
            }
        }
        [SerializeField]
        private int _meshLength = MIN_MESH_SIZE * 2;

        public float MeshScale
        {
            get { return _meshScale; }
            set
            {
                if (Mathf.Approximately(value, _meshScale))
                    return;

                _meshScale = Mathf.Clamp(value, MIN_MESH_SCALE, float.MaxValue);
                BuildMeshBase();
                BuildMeshCollider();
            }
        }
        [SerializeField]
        private float _meshScale = 1;
        #endregion

        #region Maps
        public int MapsResolution
        {
            get { return _mapsResolution; }
            set
            {
                if (value == _mapsResolution)
                    return;

                _mapsResolution = Mathf.Clamp(value, 1, MAX_HEIGHTMAP_RESOLUTION);
                BuildAllMaps();
            }
        }
        [SerializeField]
        private int _mapsResolution = 4;

        public MapQuality MapsQuality = MapQuality.High; 

        public float MaxOffsetValue
        {
            get { return _maxOffsetValue; }
            set
            {
                _maxOffsetValue = Mathf.Clamp(value, 0.01f, float.MaxValue);
                SourceMaterial.SetFloat("_offset_max", _maxOffsetValue);
                UpdateMeshBounds();
            }
        }
        [SerializeField]
        private float _maxOffsetValue = 0.5f;

        public float TextureSize
        {
            get { return _textureSize; }
            set
            {
                if (Mathf.Approximately(value, _textureSize))
                    return;

                Vector2 tiling = new Vector2(1 / value, 1 / value);

                for (int i = 0; i < SHADER_TEX_NAMES.Length; i++)
                    SourceMaterial.SetTextureScale(SHADER_TEX_NAMES[i], tiling);

                _textureSize = value;
            }
        }
        [SerializeField]
        private float _textureSize = 1;

        public float TextureBlendHeight
        {
            get { return _blendHeight; }
            set
            {
                _blendHeight = Mathf.Clamp(value, 0.01f, float.MaxValue);
                SourceMaterial.SetFloat("_blend_height", _blendHeight);
            }
        }
        [SerializeField]
        private float _blendHeight = 1;
        #endregion

        #region Collision
        public LayerMask CollisionMask
        {
            get { return _collisionMask; }
            set { _collisionMask = value; }
        }
        [SerializeField]
        private LayerMask _collisionMask = ~0;

        public float DeformSpeed
        {
            get { return _deformSpeed; }
            set { _deformSpeed = Mathf.Clamp01(value); }
        }
        [SerializeField]
        private float _deformSpeed = 1;

        public bool Smoothing = true;

        public int SmoothingRadius
        {
            get { return _smoothingRadius; }
            set { _smoothingRadius = Mathf.Clamp(value, 0, DeformableSurface.BLUR_WEIGHTS.GetLength(0) - 1); }
        }
        [SerializeField]
        private int _smoothingRadius = 3;

        public int SmoothingPasses
        {
            get { return _smoothingPasses; }
            set { _smoothingPasses = Mathf.Clamp(value, 1, MAX_SMOOTH_PASSES); }
        }
        [SerializeField]
        private int _smoothingPasses = 1;
        #endregion
        
        #region Asset sources
        public Mesh MeshBase;
        public Mesh MeshCollider;

        public Texture2D HeightMap;
        public Texture2D OffsetMap;
        public Texture2D NormalMap;

        public Shader DepthTextureShader
        {
            get
            {
                if (_depthTextureShader == null)
                    _depthTextureShader = Shader.Find("Deformable Surface/Depth Texture");
                return _depthTextureShader;
            }
            set { _depthTextureShader = value; }
        }
        [SerializeField]
        private Shader _depthTextureShader;

        public ComputeShader SurfaceKernels;
        public Material SourceMaterial;

        public Terrain TargetTerrain;
        #endregion

        #region Process
        public int GetMapsWidth { get { return MeshWidth * MapsResolution; } }
        public int GetMapsHeight { get { return MeshLength * MapsResolution; } }

        public float MaxHeightValue
        {
            get { return _maxHeightValue; }
            set { _maxHeightValue = Mathf.Clamp(value, 0, float.MaxValue); }
        }
        [SerializeField]
        private float _maxHeightValue;
        #endregion


        public void BuildAllMaps()
        {
            BuildHeightMap();
            BuildOffsetMap();
            UpdateMaxHeightValue();
            BuildNormalMap();

            UpdateMeshBounds();
        }

        public void BuildMeshBase()
        {
            MeshBase.Clear();

            SurfaceMeshParams p = new SurfaceMeshParams(MeshWidth, MeshLength, MeshScale);

            MeshBase.vertices = SurfaceMeshBuilder.GetVertices(p);
            MeshBase.triangles = SurfaceMeshBuilder.GetTriangles(p);
            MeshBase.tangents = SurfaceMeshBuilder.GetTangents(p);
            MeshBase.uv = SurfaceMeshBuilder.GetUV1(p);
            MeshBase.uv2 = SurfaceMeshBuilder.GetUV2(p);

            UpdateMeshBounds();
            MeshBase.RecalculateNormals();
        }

        public void BuildMeshCollider()
        {
            MeshCollider.Clear();

            SurfaceMeshParams p = new SurfaceMeshParams(MeshWidth, MeshLength, MeshScale);

            MeshCollider.vertices = SurfaceMeshBuilder.GetVertices(p, HeightMap, MapsResolution);
            MeshCollider.triangles = SurfaceMeshBuilder.GetTriangles(p);
            MeshCollider.tangents = SurfaceMeshBuilder.GetTangents(p);

            MeshCollider.RecalculateNormals();
        }

        public void BuildHeightMap()
        {
            HeightMap.Resize(GetMapsWidth, GetMapsHeight);

            Color[] colors = new Color[HeightMap.width * HeightMap.height];
            
            HeightMap.SetPixels(colors);
            HeightMap.Apply();

            MaxHeightValue = 0;
            BuildMeshCollider();
        }

        public void BuildOffsetMap()
        {
            int w = GetMapsWidth;
            int h = GetMapsHeight;

            OffsetMap.Resize(w, h);

            Color[] colors = new Color[w * h];

            int colId = 0;
            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w; j++)
                {
                    if (i != 0 && i != h - 1 && j != 0 && j != w - 1)
                        colors[colId].r = MaxOffsetValue;
                    colId++;
                }
            }
            
            OffsetMap.SetPixels(colors);
            OffsetMap.Apply();
        }

        public void BuildNormalMap()
        {
            NormalMap.Resize(HeightMap.width, HeightMap.height);
            
            RenderTexture normalMapRt = new RenderTexture(HeightMap.width, HeightMap.height, 0)
            {
                enableRandomWrite = true
            };
            normalMapRt.Create();

            int kernelId = DeformableSurface.GEN_NORMAL_KERNEL_ID;
            SurfaceKernels.SetTexture(kernelId, "HeightMap", HeightMap);
            SurfaceKernels.SetTexture(kernelId, "OffsetMap", OffsetMap);
            SurfaceKernels.SetTexture(kernelId, "NormalRT", normalMapRt);

            SurfaceKernels.Dispatch(kernelId,
                NormalMap.width / DeformableSurface.GROUP_THREADS_COUNT,
                NormalMap.height / DeformableSurface.GROUP_THREADS_COUNT,
                1);

            RenderTexture.active = normalMapRt;

            NormalMap.ReadPixels(new Rect(0, 0, normalMapRt.width, normalMapRt.height), 0, 0);
            NormalMap.Apply();

            RenderTexture.active = null;
            DestroyImmediate(normalMapRt);
        }


        public void UpdateHeightDependecies()
        {
            UpdateMaxHeightValue();
            UpdateMeshBounds();
            BuildMeshCollider();
        }

        public void UpdateMaxHeightValue()
        {
            float maxValue = 0;
            for (int i = 0; i < HeightMap.width; i++)
            {
                for (int j = 0; j < HeightMap.height; j++)
                {
                    float heightValue = HeightMap.GetPixel(i, j).r;
                    if (maxValue < heightValue)
                        maxValue = heightValue;
                }
            }

            MaxHeightValue = maxValue;
        }

        public void UpdateMeshBounds()
        {
            MeshBase.bounds = SurfaceMeshBuilder.GetBounds(
                new SurfaceMeshParams(MeshWidth, MeshLength, MeshScale),
                MaxHeightValue + MaxOffsetValue);
        }

        public void FlattenMap(SurfaceMapType mapType, float value)
        {
            value = Mathf.Clamp(value, 0, float.MaxValue);

            Texture2D map = mapType == SurfaceMapType.Height ? HeightMap : OffsetMap;

            for (int i = 0; i < map.width; i++)
                for (int j = 0; j < map.height; j++)
                    map.SetPixel(i, j, new Color(value, 0, 0, 0));

            map.Apply();

            if (mapType == SurfaceMapType.Height)
                UpdateHeightDependecies();

            BuildNormalMap();
        }

        public void RandomizeMap(SurfaceMapType mapType, int seed, float multiplier, float maxValue)
        {
            Texture2D map = mapType == SurfaceMapType.Height ? HeightMap : OffsetMap;
            float maxClamp = mapType == SurfaceMapType.Height ? float.MaxValue : MaxOffsetValue;

            int w = map.width;
            int h = map.height;

            Color[] colors = new Color[w * h];

            int colId = 0;
            float noiseMul = multiplier / MapsResolution;
            for (int i = 0; i < h; i++)
            {
                for (int j = 0; j < w; j++)
                {
                    if (i != 0 && i != h - 1 && j != 0 && j != w - 1)
                        colors[colId].r = Mathf.Clamp(Mathf.PerlinNoise(j * noiseMul + seed, i * noiseMul + seed) * maxValue, 0, maxClamp);
                    colId++;
                }
            }
            map.SetPixels(colors);
            map.Apply();

            if (mapType == SurfaceMapType.Height)
                UpdateHeightDependecies();

            BuildNormalMap();
        }

    }

    public enum SurfaceMapType
    {
        Height = 0, Offset
    }

    public enum MapQuality
    {
        High = 14, Medium = 15, Low = 16
    }
}


﻿using UnityEngine;
using System.Collections;

namespace DeformableSurfaceExtensions
{
    [RequireComponent(typeof(Camera))]
    public class DepthCamera : MonoBehaviour
    {
        private Material _depthMat;
        private DeformableSurface _sourceSurface;
        private Camera _camera;

        void Awake()
        {
            _camera = GetComponent<Camera>();
        }

        public void Init(DeformableSurface sourceSurface)
        {
            _sourceSurface = sourceSurface;
            _depthMat = new Material(_sourceSurface.Data.DepthTextureShader);
        }

        void OnRenderImage(RenderTexture src, RenderTexture dest)
        {
            if (!_sourceSurface.DeformEnabled)
                return;

            Graphics.Blit(src, dest, _depthMat);
            _sourceSurface.Deform();
        }

        void OnPreCull()
        {
            _camera.ResetWorldToCameraMatrix();
            _camera.ResetProjectionMatrix();
            _camera.projectionMatrix = _camera.projectionMatrix * Matrix4x4.Scale(new Vector3(1, -1, 1));
        }

        void OnPreRender()
        {
            GL.invertCulling = true;
        }

        void OnPostRender()
        {
            GL.invertCulling = false;
        }
    }
}


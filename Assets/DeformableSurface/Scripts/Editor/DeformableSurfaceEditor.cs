﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

namespace DeformableSurfaceExtensions
{
    [CustomEditor(typeof(DeformableSurface))]
    public class DeformableSurfaceEditor : Editor
    {
        #region Const
        const string NEW_SURFACE_NAME = "New Deformable Surface";
        const string GENERAL_ASSET_NAME = "DeformableSurface";
        const string COMPUTE_ASSET_NAME = "SurfaceKernels";
        const string COMPUTE_EDITOR_ASSET_NAME = "SurfaceKernelsEditor";
        const string MENU_ITEM_CREATE_PATH = "GameObject/3D Object/Deformable Surface";
        const string BRUSH_TEXTURE_NAME = "DeformableSurface_brush";

        const int COMPUTE_PAINT_KERNEL_ID = 0;

        static readonly string[,] DEFAULT_TEX_NAMES =
        {
            { SurfaceData.SHADER_TEX_NAMES[0], "DeformableSurface_snow_base_albedo" },
            { SurfaceData.SHADER_TEX_NAMES[1], "DeformableSurface_snow_flat_albedo" },
            { SurfaceData.SHADER_TEX_NAMES[2], "DeformableSurface_snow_base_normal" },
            { SurfaceData.SHADER_TEX_NAMES[3], "DeformableSurface_snow_flat_normal" },
        };

        static readonly DebugMessage[] EDITOR_MESSAGES =
        {
            new DebugMessage("One or more shader assets was missed", MessageType.Error), //0
            new DebugMessage("Compute shader was not found", MessageType.Error), //1
            new DebugMessage("DeformableSurfaceEditor has lost reference to this object", MessageType.Error), //2
            new DebugMessage("Reference to SurfaceData is missing", MessageType.Error), //3
            new DebugMessage("DepthTexture shader was not found", MessageType.Error), //4
            new DebugMessage("Changing mesh width, lenght and maps resolution will clear heightmap data", MessageType.Info), //5
            new DebugMessage(BRUSH_TEXTURE_NAME + " texture was not found. Painting will not be permitted", MessageType.Warning), //6
            new DebugMessage("This will clear all manual changes done in height map. Continue anyway?", MessageType.Info), //7
            new DebugMessage("This will clear all manual changes done in offset map. Continue anyway?", MessageType.Info), //8
            new DebugMessage("Reference to Deformable Surface object is missing", MessageType.Error), //9
            new DebugMessage("[CTRL + SCROLL WHELL] Adjust brush radius", MessageType.Info), //10
            new DebugMessage("[SHIFT + SCROLL WHEEL] Adjust brush hardness", MessageType.Info), //11
            new DebugMessage("Target terrain is too small for alignment", MessageType.Info), //12
            new DebugMessage("Unable to load inspector", MessageType.Warning), //13
        };

        static readonly GUIContent[] FIELDS_CONTENT =
        {
            new GUIContent("Width", "Number of polygons by width (X) of the base mesh"), //0
            new GUIContent("Length", "Number of polygons by length (Z) of the base mesh"), //1
            new GUIContent("Scale", "Scale of the base mesh"), //2
            new GUIContent("Resolution", "This value will be multiplied by mesh width and length to determine the final width and height of all texture maps"), //3
            new GUIContent("Max Offset Value", "Y offset of the deformable part is clamped between height map values and this value"), //4
            new GUIContent("Collision Mask", "This layer mask is used to determine which objects on the scene may deform the surface"), //5
            new GUIContent("Deform Speed", "Framerate dependent deformation speed of the surface"), //6
            new GUIContent("Smoothing", "Is smoothing should be applied to deformation process? Note that this feature requires additional compute shader passes"), //7
            new GUIContent("Radius", "Smoothing radius in pixels"), //8
            new GUIContent("Passes", "Number of smoothing passes. It is also creates additional compute shader passes"), //9
            new GUIContent("Quality", "Tesselation quality. Higher value increases triangles count of the surface"), //10
            new GUIContent("Distance", "Tesselation distance. Use this value as some kind of LOD implementation"), //11
            new GUIContent("Quality", "Maps quality. Affects on precision of deformation calculations. " +
                                      "In fact, it is just Render Texture type. Choose between High (RFloat), Medium (RHalf) and Low (R8)"), //12
        };

        static readonly string[] DIALOG_BTN =
        {
            "Yes", "No", "Yes and never ask again"
        };

        #endregion

        #region Editor fields
        private bool _editorReady;

        private GUIContent[] _toolbarContent;
        private string[] _paintToolbarContent;

        private ComputeShader _computeEditor;
        private Texture2D _brushAlpha;
        private Material _brushMaterial;
        private RenderTexture _brushRt;
        private Texture2D _tempMap;

        private int _selectedToolbar;
        
        private SurfaceMapType _paintMode;
        private int _brushRadius;
        private float _brushHardness;

        private float _flattenValue;

        private float _rndPerlinSize;
        private float _rndPerlinMaxValue;
        private int _rndPerlinSeed;

        private float _alignOffset;

        private bool _dialog_neverAskFlatten;
        private bool _dialog_neverAskRnd;
        #endregion

        private DeformableSurface _target;

        void OnEnable()
        {
            _target = (DeformableSurface)target;
            if (_target == null)
            {
                DebugMessage(9);
                return;
            }
            if (_target.Data == null)
            {
                DebugMessage(3);
                return;
            }
            _computeEditor = GetCompute(COMPUTE_EDITOR_ASSET_NAME);
            if (_computeEditor == null)
                return;

            //Load GUI content
            _toolbarContent = new[]
            {
                EditorGUIUtility.IconContent("TerrainInspector.TerrainToolSetHeight", "Paint height"),
                EditorGUIUtility.IconContent("TerrainInspector.TerrainToolSplat", "Textures"),
                EditorGUIUtility.IconContent("TerrainInspector.TerrainToolSettings", "Settings")
            };
            _paintToolbarContent = new[]
            {
                 "Height", "Offset"
            };

            LoadPrefs();
            LoadBrush();

            #if !UNITY_5_5_OR_NEWER
            EditorUtility.SetSelectedWireframeHidden(_target.GetComponent<Renderer>(), true);
            #endif

            _editorReady = true;
        }

        void LoadBrush()
        {
            _brushAlpha = GetBrushAlpha();

            _brushMaterial = new Material(Shader.Find("Deformable Surface/Surface Editor Brush"));

            _brushMaterial.SetTexture(SurfaceData.SHADER_MAPS_TEX_NAMES[0], _target.Data.HeightMap);
            _brushMaterial.SetTexture(SurfaceData.SHADER_MAPS_TEX_NAMES[1], _target.Data.OffsetMap);

            if (_selectedToolbar == 0)
                InitBrushResources();
        }

        void InitBrushResources()
        {
            int w = _target.Data.HeightMap.width;
            int h = _target.Data.HeightMap.height;

            _brushRt = new RenderTexture(w, h, 0, RenderTextureFormat.ARGBFloat)
            {
                name = "BrushRt",
                filterMode = FilterMode.Point,
                autoGenerateMips = false,
                wrapMode = TextureWrapMode.Clamp,
                enableRandomWrite = true
            };

            _tempMap = new Texture2D(w, h, TextureFormat.RGBAFloat, false)
            {
                name = "TempMap",
                filterMode = FilterMode.Bilinear,
                wrapMode = TextureWrapMode.Clamp
            };

            _brushMaterial.SetFloat("_tess_distance", _target.Data.SourceMaterial.GetFloat("_tess_distance"));
            _brushMaterial.SetFloat("_tess_amount", _target.Data.SourceMaterial.GetFloat("_tess_amount"));
            _brushMaterial.SetTexture("_brush", _brushRt);
        }

        void LoadPrefs()
        {
            _selectedToolbar = Mathf.Clamp(EditorPrefs.GetInt("_selectedToolbar", 0), 0, _toolbarContent.Length - 1);
            _paintMode = (SurfaceMapType)Mathf.Clamp(EditorPrefs.GetInt("_selectedPaintMode", 0), 0, 1);
            _brushRadius = EditorPrefs.GetInt("_brushRadius", 4);
            _brushHardness = Mathf.Clamp(EditorPrefs.GetFloat("_brushHardness", 0.5f), 0.001f, 1f);
            _flattenValue = Mathf.Clamp(EditorPrefs.GetFloat("_flattenValue", 1), 0, float.MaxValue);
            _rndPerlinSize = EditorPrefs.GetFloat("_rndPerlinSize", 0.5f);
            _rndPerlinMaxValue = EditorPrefs.GetFloat("_rndPerlinMaxValue", 1);
            _rndPerlinSeed = EditorPrefs.GetInt("_rndPerlinSeed", 123456);
            _alignOffset = EditorPrefs.GetFloat("_alignOffset", 0.1f);

            _dialog_neverAskFlatten = EditorPrefs.GetBool("_dialog_neverAskFlatten", false);
            _dialog_neverAskRnd = EditorPrefs.GetBool("_dialog_neverAskRnd", false);
        }

        void SavePrefs()
        {
            EditorPrefs.SetInt("_selectedToolbar", _selectedToolbar);
            EditorPrefs.SetInt("_selectedPaintMode", (int)_paintMode);
            EditorPrefs.SetInt("_brushRadius", _brushRadius);
            EditorPrefs.SetFloat("_brushHardness", _brushHardness);
            EditorPrefs.SetFloat("_flattenValue", _flattenValue);
            EditorPrefs.SetFloat("_rndPerlinSize", _rndPerlinSize);
            EditorPrefs.SetFloat("_rndPerlinMaxValue", _rndPerlinMaxValue);
            EditorPrefs.SetInt("_rndPerlinSeed", _rndPerlinSeed);
            EditorPrefs.SetFloat("_alignOffset", _alignOffset);

            EditorPrefs.SetBool("_dialog_neverAskFlatten", _dialog_neverAskFlatten);
            EditorPrefs.SetBool("_dialog_neverAskRnd", _dialog_neverAskRnd);
        }
        
        void OnDisable()
        {
            if (!_editorReady)
                return;

            SavePrefs();
            DestroyImmediate(_brushRt);
            DestroyImmediate(_brushMaterial);
        }
        

        public override void OnInspectorGUI()
        {
            #region Check
            if (_target == null)
            {
                DrawMessage(2);
                return;
            }

            SurfaceData data = _target.Data;
            if (data == null)
            {
                _target.Data = (SurfaceData)EditorGUILayout.ObjectField("Surface Data", _target.Data, typeof(SurfaceData), false);
                DrawMessage(3);
                return;
            }

            if (!_editorReady)
            {
                DrawMessage(13);
                return;
            }
            #endregion

            DrawToolbar();
            UseBaseHotKeys(Event.current);
            switch (_selectedToolbar)
            {
                case 0:
                    #region Draw paint settings
                    DrawLabel("Paint Mode");
                    _paintMode = (SurfaceMapType) GUILayout.Toolbar((int) _paintMode, _paintToolbarContent);
                    
                    DrawLabel("Brush");
                    _brushRadius = EditorGUILayout.IntSlider("Radius", _brushRadius, 1, 256);
                    _brushHardness = EditorGUILayout.Slider("Hardness", _brushHardness, 0.001f, 1f);

                    EditorGUI.BeginDisabledGroup(Application.isPlaying);
                    DrawLabel("Flatten");

                    _flattenValue = Mathf.Clamp(EditorGUILayout.FloatField("Target Value", _flattenValue), 0,
                        _paintMode == SurfaceMapType.Height ? float.MaxValue : data.MaxOffsetValue);

                    DrawFlattenButton();

                    EditorGUILayout.Space();
                    EditorGUILayout.LabelField("Randomizer", EditorStyles.boldLabel);

                    _rndPerlinSize = EditorGUILayout.FloatField("Size", _rndPerlinSize);
                    _rndPerlinMaxValue = EditorGUILayout.FloatField("Max Value", _rndPerlinMaxValue);

                    int perlinSeed = _rndPerlinSeed;
                    perlinSeed = EditorGUILayout.DelayedIntField("Seed", perlinSeed);
                    if (perlinSeed != _rndPerlinSeed)
                    {
                        _rndPerlinSeed = perlinSeed;
                        RndMap(false);
                    }
                    DrawRndButton();

                    DrawLabel("Terrain Alignment");
                    data.TargetTerrain = (Terrain) EditorGUILayout.ObjectField("Target Terrain", data.TargetTerrain, typeof(Terrain), true);
                    _alignOffset = EditorGUILayout.FloatField("Height Offset", _alignOffset);

                    bool noTerrain = data.TargetTerrain == null || data.TargetTerrain.terrainData == null;
                    EditorGUI.BeginDisabledGroup(noTerrain);

                    if (GUILayout.Button("Align"))
                        AlignToTerrain(data);

                    if (noTerrain)
                        EditorGUI.EndDisabledGroup();

                    EditorGUI.EndDisabledGroup();
                    #endregion
                    break;

                case 1:
                    #region Draw material settings
                    DrawLabel("Material Settings");
                    data.TextureSize = EditorGUILayout.FloatField("Texture Size", data.TextureSize);
                    data.TextureBlendHeight = EditorGUILayout.FloatField("Texture Blend Height", data.TextureBlendHeight);

                    EditorGUILayout.Space();
                    DrawMatSlider(new GUIContent("Metallic"), "_metallic", 0, 1);
                    DrawMatSlider(new GUIContent("Smoothness"), "_smoothness", 0, 1);

                    DrawLabel("Material Textures");
                    float boxSize = 100;
                    EditorGUILayout.LabelField("Albedo (RGB) Smoothness (A)");
                    Rect layourRect = EditorGUILayout.GetControlRect();
                    layourRect.y += 5;

                    DrawTextureBox(SurfaceData.SHADER_TEX_NAMES[0], new Rect(layourRect.x, layourRect.y, boxSize, boxSize), true);
                    DrawTextureBox(SurfaceData.SHADER_TEX_NAMES[1], new Rect(layourRect.x + boxSize + 10, layourRect.y, boxSize, boxSize), false);

                    EditorGUILayout.LabelField("Normal Map (RGB)");
                    layourRect = EditorGUILayout.GetControlRect();
                    layourRect.y += 5;

                    DrawTextureBox(SurfaceData.SHADER_TEX_NAMES[2], new Rect(layourRect.x, layourRect.y, boxSize, boxSize), true);
                    DrawTextureBox(SurfaceData.SHADER_TEX_NAMES[3], new Rect(layourRect.x + boxSize + 10, layourRect.y, boxSize, boxSize), false);
                    #endregion
                    break;

                case 2:
                    #region Draw surface settings
                    EditorGUI.BeginDisabledGroup(Application.isPlaying);
                    EditorGUI.BeginChangeCheck();

                    EditorGUILayout.Space();
                    _target.Data = (SurfaceData)EditorGUILayout.ObjectField("Surface Data", _target.Data, typeof(SurfaceData), false);

                    EditorGUI.EndDisabledGroup();
                    _target.DeformEnabled = EditorGUILayout.Toggle("Deform Enabled", _target.DeformEnabled);
                    EditorGUI.BeginDisabledGroup(Application.isPlaying);

                    DrawLabel("Mesh");
                    data.MeshWidth = EditorGUILayout.DelayedIntField(FIELDS_CONTENT[0], data.MeshWidth);
                    data.MeshLength = EditorGUILayout.DelayedIntField(FIELDS_CONTENT[1], data.MeshLength);
                    data.MeshScale = EditorGUILayout.DelayedFloatField(FIELDS_CONTENT[2], data.MeshScale);

                    DrawLabel("Maps");
                    data.MapsResolution = EditorGUILayout.DelayedIntField(FIELDS_CONTENT[3], data.MapsResolution);
                    data.MapsQuality = (MapQuality) EditorGUILayout.EnumPopup(FIELDS_CONTENT[12], data.MapsQuality);
                    data.MaxOffsetValue = EditorGUILayout.FloatField(FIELDS_CONTENT[4], data.MaxOffsetValue);

                    EditorGUILayout.HelpBox(EDITOR_MESSAGES[5].Message, EDITOR_MESSAGES[5].Type);

                    DrawLabel("Deformation");
                    data.CollisionMask = LayerMaskField(FIELDS_CONTENT[5], data.CollisionMask);
                    data.DeformSpeed = EditorGUILayout.Slider(FIELDS_CONTENT[6], data.DeformSpeed, 0, 1);
                    data.Smoothing = EditorGUILayout.Toggle(FIELDS_CONTENT[7], data.Smoothing);
                    if (data.Smoothing)
                    {
                        EditorGUI.indentLevel = 1;
                        data.SmoothingRadius = EditorGUILayout.IntSlider(FIELDS_CONTENT[8], data.SmoothingRadius + 1, 1, DeformableSurface.BLUR_WEIGHTS.GetLength(0)) - 1;
                        data.SmoothingPasses = EditorGUILayout.IntSlider(FIELDS_CONTENT[9], data.SmoothingPasses, 1, SurfaceData.MAX_SMOOTH_PASSES);
                        EditorGUI.indentLevel = 0;
                    }

                    EditorGUI.EndDisabledGroup();

                    DrawLabel("Tesselation");
                    DrawMatSliderInt(FIELDS_CONTENT[10], "_tess_amount", 1, SurfaceData.MAX_TESS_QUALITY);

                    float tessDist = data.SourceMaterial.GetFloat("_tess_distance");
                    EditorGUI.BeginChangeCheck();
                    tessDist = EditorGUILayout.FloatField(FIELDS_CONTENT[11], tessDist);
                    if (EditorGUI.EndChangeCheck())
                    {
                        tessDist = Mathf.Clamp(tessDist, 0, int.MaxValue);
                        data.SourceMaterial.SetFloat("_tess_distance", tessDist);
                    }

                    if (EditorGUI.EndChangeCheck())
                        RefreshMeshCollider();
                    #endregion
                    break;
            }
        }

        void OnSceneGUI()
        {
            #region Check
            if (_target == null || !_editorReady)
                return;
            SurfaceData data = _target.Data;
            if (data == null)
                return;
            Camera sceneViewCamera = GetSceneViewCamera();
            if (sceneViewCamera == null)
                return;
            #endregion

            #region Draw bounds
            if (_target.Data.MeshBase != null)
            {
                Handles.color = new Color(1, 1, 1, 0.25f);
                Handles.DrawWireCube(_target.Data.MeshBase.bounds.center + _target.transform.position, _target.Data.MeshBase.bounds.size);
            }
            #endregion

            Event e = Event.current;

            bool showBrush = _selectedToolbar == 0 && Tools.current == Tool.None && !Application.isPlaying;
            if (showBrush)
            {
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

                Vector3 pointerPos = GetPointerPosition(sceneViewCamera);
                Vector2 uvPos = GetUvFromPointer(pointerPos);

                #region Use HotKeys / Help GUI
                if (e.control)
                {
                    if (e.type == EventType.ScrollWheel)
                    {
                        _brushRadius += (int)-e.delta.y;
                        e.Use();
                        Repaint();
                    }

                    Handles.BeginGUI();
                    GUILayout.Label(EDITOR_MESSAGES[10].Message, EditorStyles.whiteLabel);
                    Handles.EndGUI();
                }
                if (e.shift)
                {
                    if (e.type == EventType.ScrollWheel)
                    {
                        _brushHardness += (-e.delta.y * 0.01f);
                        e.Use();
                        Repaint();
                    }

                    Handles.BeginGUI();
                    GUILayout.Label(EDITOR_MESSAGES[11].Message, EditorStyles.whiteLabel);
                    Handles.EndGUI();
                }
                UseBaseHotKeys(e);
                #endregion

                #region Draw brush
                _brushMaterial.SetFloat("_brush_hardness", _brushHardness);
                if (_brushMaterial.SetPass(0))
                    Graphics.DrawMeshNow(data.MeshBase, _target.transform.position + new Vector3(0, 0.05f, 0), _target.transform.rotation);

                int w = data.HeightMap.width;
                int h = data.HeightMap.height;

                Vector2 brushPos = new Vector2((uvPos.x * w) - _brushRadius * 0.5f, h - (uvPos.y * h + _brushRadius * 0.5f));
                Rect brushRect = new Rect(brushPos, new Vector2(_brushRadius, _brushRadius));

                RenderTexture.active = _brushRt;
                GL.Clear(true, true, Color.clear, 0);

                GL.PushMatrix();
                GL.LoadPixelMatrix(0, w, h, 0);
                Graphics.DrawTexture(brushRect, _brushAlpha);
                GL.PopMatrix();

                RenderTexture.active = null;
                #endregion

                #region Paint
                bool canPaint =
                    (e.type == EventType.MouseDrag || e.type == EventType.MouseDown)
                    && e.button == 0 && !e.control && !e.alt;

                if (canPaint)
                {
                    Texture2D targetMap = _paintMode == SurfaceMapType.Height ? data.HeightMap : data.OffsetMap;
                    float paintDir = (e.shift ? -_brushHardness : _brushHardness) * 0.5f;
                    float maxValue = _paintMode == SurfaceMapType.Height ? float.MaxValue : data.MaxOffsetValue;

                    _computeEditor.SetTexture(COMPUTE_PAINT_KERNEL_ID, "DepthRT", _brushRt);
                    _computeEditor.SetTexture(COMPUTE_PAINT_KERNEL_ID, "HeightMap", targetMap);
                    _computeEditor.SetFloat("DeformSpeed", paintDir);
                    _computeEditor.SetFloat("MaxHeightValue", maxValue);

                    _computeEditor.Dispatch(COMPUTE_PAINT_KERNEL_ID, 
                        w / DeformableSurface.GROUP_THREADS_COUNT, 
                        h / DeformableSurface.GROUP_THREADS_COUNT, 1);

                    RenderTexture.active = _brushRt;
                    _tempMap.ReadPixels(new Rect(0, 0, w, h), 0, 0);
                    _tempMap.Apply();
                    RenderTexture.active = null;

                    for (int i = 0; i < w; i++)
                    {
                        for (int j = 0; j < h; j++)
                        {
                            targetMap.SetPixel(i, j, _tempMap.GetPixel(i, j));
                        }
                    }
                    targetMap.Apply();
                    
                    if (_paintMode == SurfaceMapType.Height)
                    {
                        data.UpdateHeightDependecies();
                        RefreshMeshCollider();
                    }
                    data.BuildNormalMap();
                }
                #endregion

                SceneView.RepaintAll();
            }

            _target.transform.rotation = Quaternion.identity;
            _target.transform.localScale = Vector3.one;
        }

        void UseBaseHotKeys(Event e)
        {
            if (e.keyCode == KeyCode.F1 || e.keyCode == KeyCode.F2 || e.keyCode == KeyCode.F3)
            {
                _selectedToolbar = (int)e.keyCode - 282;

                e.Use();
                Repaint();
            }

            if (e.keyCode == KeyCode.Escape)
                Tools.current = Tool.Move;
        }

        void RefreshMeshCollider()
        {
            if (!_target.MeshCollider.enabled)
                return;

            _target.MeshCollider.enabled = false;
            _target.MeshCollider.enabled = true;
        }

        Vector2 GetUvFromPointer(Vector3 pointerPos)
        {
            Vector3 localMapPos = (_target.transform.InverseTransformPoint(pointerPos) * _target.Data.MapsResolution) / _target.Data.MeshScale;
            return new Vector2(localMapPos.x / _target.Data.HeightMap.width, localMapPos.z / _target.Data.HeightMap.height);
        }

        Vector3 GetPointerPosition(Camera camera)
        {
            Ray mouseRay = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

            Plane plane = new Plane(Vector3.up, _target.transform.position);
            float distance;
            plane.Raycast(mouseRay, out distance);

            return camera.transform.position + mouseRay.direction * distance;
        }


        void FlattenMap()
        {
            _target.Data.FlattenMap(_paintMode, _flattenValue);
            if (_paintMode == SurfaceMapType.Height)
                RefreshMeshCollider();
        }
        
        void RndMap(bool generateSeed)
        {
            if (generateSeed)
                _rndPerlinSeed = UnityEngine.Random.Range(0, SurfaceData.MAX_PERLIN_SEED);

            _target.Data.RandomizeMap(_paintMode, _rndPerlinSeed, _rndPerlinSize, _rndPerlinMaxValue);
        }

        void AlignToTerrain(SurfaceData data)
        {
            Terrain t = data.TargetTerrain;
            TerrainData tData = t.terrainData;
            Texture2D heightmap = data.HeightMap;
            Vector3 meshSize = data.MeshBase.bounds.size;

            if (tData.size.x < meshSize.x || tData.size.z < meshSize.z)
            {
                DebugMessage(12);
                return;
            }

            Vector3 sPos = _target.transform.position;
            Vector3 tPos = t.gameObject.transform.position;

            int w = data.HeightMap.width;
            int h = data.HeightMap.height;

            #region Align position
            if (sPos.x < tPos.x)
                sPos.x = tPos.x;
            if (sPos.x + meshSize.x > tPos.x + tData.size.x)
                sPos.x = tPos.x + tData.size.x - meshSize.x;

            if (sPos.z < tPos.z)
                sPos.z = tPos.z;
            if (sPos.z + meshSize.z > tPos.z + tData.size.z)
                sPos.z = tPos.z + tData.size.z - meshSize.z;

            #endregion

            #region Calculate height values
            Vector3 localPos = -(tPos - sPos);
            Vector2 relSize = new Vector2(meshSize.x / tData.size.x, meshSize.z / tData.size.z);
            Vector2 startPoint = new Vector2(localPos.x / tData.size.x, localPos.z / tData.size.z);
            Vector2 endPoint = startPoint + relSize;

            float[,] heightValues = new float[w, h];

            
            float minValue = float.MaxValue;
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    Vector2 curPoint = new Vector2(
                        Mathf.Lerp(startPoint.x, endPoint.x, (float)i / w), 
                        Mathf.Lerp(startPoint.y, endPoint.y, (float)j / h));

                    float curValue = tData.GetInterpolatedHeight(curPoint.x, curPoint.y);

                    if (curValue < minValue)
                        minValue = curValue;

                    heightValues[i, j] = curValue;
                }
            }
            #endregion

            sPos.y = t.transform.position.y + minValue;
            
            for (int i = 0; i < w; i++)
                for (int j = 0; j < h; j++)
                    heightmap.SetPixel(i, j, new Color(heightValues[i, j] - minValue + _alignOffset, 0, 0, 0));

            heightmap.Apply();

            data.UpdateHeightDependecies();
            data.BuildNormalMap();
            RefreshMeshCollider();

            _target.transform.position = sPos;
        }


        #region Inspector helpers
        void DrawToolbar()
        {
            int toolbarId = _selectedToolbar;
            toolbarId = GUILayout.Toolbar(toolbarId, _toolbarContent);

            if (toolbarId != _selectedToolbar)
            {
                if (toolbarId == 0)
                {
                    Tools.current = Tool.None;
                    InitBrushResources();
                }
            }

            _selectedToolbar = toolbarId;
        }

        void DrawLabel(string label)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
        }

        void DrawFlattenButton()
        {
            string title = "Flatten " + _paintMode;

            if (!GUILayout.Button(title))
                return;

            if (_dialog_neverAskFlatten)
            {
                FlattenMap();
                return;
            }

            int dialogResult =
                EditorUtility.DisplayDialogComplex(
                    title,
                    EDITOR_MESSAGES[7 + (int)_paintMode].Message,
                    DIALOG_BTN[0],
                    DIALOG_BTN[1],
                    DIALOG_BTN[2]);

            if (dialogResult == 0)
            {
                FlattenMap();
                return;
            }

            if (dialogResult == 1)
                return;

            FlattenMap();
            _dialog_neverAskFlatten = true;
        }

        void DrawRndButton()
        {
            string title = "Randomize " + _paintMode;

            if (!GUILayout.Button(title))
                return;

            if (_dialog_neverAskRnd)
            {
                RndMap(true);
                return;
            }

            int dialogResult =
                EditorUtility.DisplayDialogComplex(
                    title,
                    EDITOR_MESSAGES[7 + (int)_paintMode].Message,
                    DIALOG_BTN[0],
                    DIALOG_BTN[1],
                    DIALOG_BTN[2]);

            if (dialogResult == 0)
            {
                RndMap(true);
                return;
            }

            if (dialogResult == 1)
                return;

            RndMap(true);
            _dialog_neverAskRnd = true;
        }

        void DrawTextureBox(string textureName, Rect rect, bool space)
        {
            if (space)
                GUILayout.Space(rect.height + 5);
            EditorGUI.BeginChangeCheck();
            Texture tex = _target.Data.SourceMaterial.GetTexture(textureName);
            tex = (Texture)EditorGUI.ObjectField(rect, tex, typeof(Texture), false);
            if (EditorGUI.EndChangeCheck())
                _target.Data.SourceMaterial.SetTexture(textureName, tex);
        }

        void DrawMatSlider(GUIContent content, string propertyName, float min, float max)
        {
            float p = _target.Data.SourceMaterial.GetFloat(propertyName);
            EditorGUI.BeginChangeCheck();
            p = EditorGUILayout.Slider(content, p, min, max);
            if (EditorGUI.EndChangeCheck())
                _target.Data.SourceMaterial.SetFloat(propertyName, p);
        }

        void DrawMatSliderInt(GUIContent content, string propertyName, int min, int max)
        {
            int p = (int)_target.Data.SourceMaterial.GetFloat(propertyName);
            EditorGUI.BeginChangeCheck();
            p = EditorGUILayout.IntSlider(content, p, min, max);
            if (EditorGUI.EndChangeCheck())
                _target.Data.SourceMaterial.SetFloat(propertyName, p);
        }
        #endregion
        
        #region Service
        Texture2D GetBrushAlpha()
        {
            string[] guids = AssetDatabase.FindAssets(BRUSH_TEXTURE_NAME + " t:" + typeof(Texture2D).Name);
            if (guids == null)
            {
                DebugMessage(1);
                return null;
            }
            if (guids.Length == 0)
            {
                DebugMessage(1);
                return null;
            }
            return AssetDatabase.LoadAssetAtPath<Texture2D>(AssetDatabase.GUIDToAssetPath(guids[0]));
        }

        Camera GetSceneViewCamera()
        {
            if (SceneView.lastActiveSceneView != null)
                if (SceneView.lastActiveSceneView.camera != null)
                    return SceneView.lastActiveSceneView.camera;

            return null;
        }

        private static List<string> _layers;
        private static string[] _layerNames;
        static LayerMask LayerMaskField(GUIContent label, LayerMask selected)
        {
            if (_layers == null)
            {
                _layers = new List<string>();
                _layerNames = new string[4];
            }
            else {
                _layers.Clear();
            }

            int emptyLayers = 0;
            for (int i = 0; i < 32; i++)
            {
                string layerName = LayerMask.LayerToName(i);

                if (layerName != "")
                {

                    for (; emptyLayers > 0; emptyLayers--)
                        _layers.Add("Layer " + (i - emptyLayers));
                    _layers.Add(layerName);
                }
                else {
                    emptyLayers++;
                }
            }

            if (_layerNames.Length != _layers.Count)
                _layerNames = new string[_layers.Count];

            for (int i = 0; i < _layerNames.Length; i++)
                _layerNames[i] = _layers[i];

            selected.value = EditorGUILayout.MaskField(label, selected.value, _layerNames);
            return selected;
        }

        static void DrawMessage(int messageId)
        {
            if (messageId < 0 || messageId >= EDITOR_MESSAGES.Length)
                return;

            EditorGUILayout.HelpBox(EDITOR_MESSAGES[messageId].Message, EDITOR_MESSAGES[messageId].Type);
        }

        static void DebugMessage(int messageId)
        {
            if (messageId < 0 || messageId >= EDITOR_MESSAGES.Length)
                return;

            switch (EDITOR_MESSAGES[messageId].Type)
            {
                case MessageType.Info:
                    Debug.Log(EDITOR_MESSAGES[messageId].Message);
                    break;
                case MessageType.Warning:
                    Debug.LogWarning(EDITOR_MESSAGES[messageId].Message);
                    break;
                case MessageType.Error:
                    Debug.LogError(EDITOR_MESSAGES[messageId].Message);
                    break;
            }
        }
        #endregion

        #region Asset builder
        [MenuItem(MENU_ITEM_CREATE_PATH)]
        public static void CreateNewSurface()
        {
            #region Create data asset
            string dataId = GetNewSurfaceDataId();
            string newDataAssetPath = "Assets/" + NEW_SURFACE_NAME + dataId + ".asset";
            SurfaceData data = CreateInstance<SurfaceData>();
            AssetDatabase.CreateAsset(data, newDataAssetPath);
            #endregion

            #region Find shaders
            Shader dsShader = Shader.Find("Deformable Surface/Deformable Surface Standard");
            Shader depthShader = Shader.Find("Deformable Surface/Depth Texture");

            if (!dsShader || !depthShader)
            {
                DebugMessage(0);
                return;
            }
            #endregion

            #region Find compute shader
            ComputeShader compute = GetCompute(COMPUTE_ASSET_NAME);
            if (compute == null)
                return;
            #endregion

            #region Create materials
            Material mat = new Material(dsShader)
            {
                name = GetFullAssetName("material", data)
            };
            AssetDatabase.AddObjectToAsset(mat, data);
            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(mat));
            #endregion

            #region Fill data
            data.MeshBase = AddNewMesh("mesh_base", data);
            data.MeshCollider = AddNewMesh("mesh_collider", data);

            data.HeightMap = AddNewTextureMap("height_map", TextureFormat.RFloat, data);
            data.OffsetMap = AddNewTextureMap("offset_map", TextureFormat.RFloat, data);
            data.NormalMap = AddNewTextureMap("normal_map", TextureFormat.ARGB32, data);
            
            data.SurfaceKernels = compute;
            data.SourceMaterial = mat;
            data.DepthTextureShader = depthShader;

            data.BuildMeshBase();
            data.BuildAllMaps();
            data.BuildMeshCollider();

            AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(data));
            #endregion

            #region Assign textures to material
            for (int i = 0; i < DEFAULT_TEX_NAMES.GetLength(0); i++)
            {
                string[] texGUIds = AssetDatabase.FindAssets(DEFAULT_TEX_NAMES[i, 1] + " t:" + typeof(Texture).Name, new[] { "Assets" });
                if (texGUIds == null || texGUIds.Length == 0)
                    continue;

                Texture tex = AssetDatabase.LoadAssetAtPath<Texture>(AssetDatabase.GUIDToAssetPath(texGUIds[0]));

                mat.SetTexture(DEFAULT_TEX_NAMES[i, 0], tex);
            }

            mat.SetTexture(SurfaceData.SHADER_MAPS_TEX_NAMES[0], data.HeightMap);
            mat.SetTexture(SurfaceData.SHADER_MAPS_TEX_NAMES[1], data.OffsetMap);
            mat.SetTexture(SurfaceData.SHADER_MAPS_TEX_NAMES[2], data.NormalMap);
            #endregion

            //Create game object
            DeformableSurface newSurface = new GameObject(NEW_SURFACE_NAME + dataId).AddComponent<DeformableSurface>();
            newSurface.Data = data;
            Undo.RegisterCreatedObjectUndo(newSurface.gameObject, "New Deformable Surface");

            EditorPrefs.SetInt("_selectedToolbar", 2);

            //Align to parent
            if (Selection.activeGameObject)
                GameObjectUtility.SetParentAndAlign(newSurface.gameObject, Selection.activeGameObject);
            Selection.activeGameObject = newSurface.gameObject;
        }

        static ComputeShader GetCompute(string fileName)
        {
            string[] guids = AssetDatabase.FindAssets(fileName + " t:" + typeof(ComputeShader).Name);
            if (guids == null)
            {
                DebugMessage(1);
                return null;
            }
            if (guids.Length == 0)
            {
                DebugMessage(1);
                return null;
            }

            ComputeShader[] shaders = new ComputeShader[guids.Length];
            for (int i = 0; i < shaders.Length; i++)
                shaders[i] = AssetDatabase.LoadAssetAtPath<ComputeShader>(AssetDatabase.GUIDToAssetPath(guids[i]));

            for (int i = 0; i < shaders.Length; i++)
                if (shaders[i] != null && shaders[i].name == fileName)
                    return shaders[i];

            return null;
        }

        static Mesh AddNewMesh(string name, SurfaceData data)
        {
            Mesh mesh = new Mesh { name = GetFullAssetName(name, data) };
            AssetDatabase.AddObjectToAsset(mesh, data);

            return mesh;
        }

        static Texture2D AddNewTextureMap(string name, TextureFormat format, SurfaceData data)
        {
            Texture2D tex = new Texture2D(4, 4, format, false)
            {
                name = GetFullAssetName(name, data),
                wrapMode = TextureWrapMode.Clamp
            };
            AssetDatabase.AddObjectToAsset(tex, data);

            return tex;
        }

        static string GetNewSurfaceDataId()
        {
            string searchFilter = NEW_SURFACE_NAME + " t:" + typeof(SurfaceData).Name;

            string[] foundDataGUIDs = AssetDatabase.FindAssets(searchFilter, new[] { "Assets" });
            string[] foundDataNames = new string[foundDataGUIDs.Length];
            List<int> foundIDs = new List<int>();

            for (int i = 0; i < foundDataGUIDs.Length; i++)
            {
                foundDataNames[i] = AssetDatabase.GUIDToAssetPath(foundDataGUIDs[i]).Replace(".asset", "");

                string[] splittedName = foundDataNames[i].Split(' ');
                int resultId;
                if (int.TryParse(splittedName[splittedName.Length - 1], out resultId))
                    foundIDs.Add(resultId);
            }

            if (foundIDs.Count == 0)
                return " 1";

            foundIDs.Sort();

            int finalId = 1;
            for (int i = 0; i < foundIDs.Count; i++)
            {
                if (finalId != foundIDs[i])
                    break;

                if (foundIDs[i] == finalId)
                    finalId++;
            }

            return " " + finalId;
        }

        static string GetFullAssetName(string name, SurfaceData data)
        {
            return string.Format("{0}_{1}_{2}", GENERAL_ASSET_NAME, name, data.GetInstanceID());
        }
#endregion
    }

    internal struct DebugMessage
    {
        public string Message;
        public MessageType Type;

        public DebugMessage(string message, MessageType type)
        {
            Message = message;
            Type = type;
        }
    }
}



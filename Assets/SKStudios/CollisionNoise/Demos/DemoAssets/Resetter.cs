﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKStudios.CollisionNoise.Demos
{
    public class Resetter : MonoBehaviour
    {
        public bool MinVelocity;
        public float minVelocity = 0.05f;
        private Vector3 _initialPos;
        private Quaternion _initialRot;

        private Rigidbody _body;
        private Rigidbody Body {
            get {
                if (!_body)
                    _body = GetComponent<Rigidbody>();
                return _body;
            }
        }

        void Start()
        {
            _initialPos = transform.position;
            _initialRot = transform.rotation;
            if (!MinVelocity)
                StartCoroutine(Reset());
        }

        void FixedUpdate()
        {
            if (MinVelocity)
            {
                if (Body.velocity.magnitude < minVelocity)
                {
                    StartCoroutine(Reset());
                }
            }
        }

        IEnumerator Reset()
        {
            if (MinVelocity)
            {
                yield return new WaitForSeconds(1f);

                if (Body.velocity.magnitude < minVelocity)
                {
                    transform.position = _initialPos;
                    transform.rotation = _initialRot;
                }

            }
            else
            {
                while (true)
                {
                    yield return new WaitForSeconds(minVelocity);
                    transform.position = _initialPos;
                    transform.rotation = _initialRot;
                }
            }

        }
    }

}

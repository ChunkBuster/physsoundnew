﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hammer : MonoBehaviour {
	// Use this for initialization
	void Start () {
	    Collider col = this.gameObject.GetComponent<Collider>();
	    col.attachedRigidbody.centerOfMass = this.gameObject.transform.position;
	}
}

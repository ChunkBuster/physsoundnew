﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {
    public bool Exploding = false;
    public float radius = 5;
    public float strength = 10;
    public float heightMod = 10;
    public ParticleSystem system;
    void FixedUpdate() {
        if (Exploding) {
            Exploding = false;
            Collider[] thrownObjects = Physics.OverlapSphere(transform.position, radius);
            foreach (Collider c in thrownObjects) {
                if(c.attachedRigidbody)
                    c.attachedRigidbody.AddExplosionForce(radius, transform.position,strength, heightMod, ForceMode.Impulse);
            }
            GetComponent<AudioSource>().Play();
            system.Play();

        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionTrigger : MonoBehaviour {
    public Explosion explosion;
    public bool triggered = false;
    public void OnTriggerEnter(Collider other) {
        if (!triggered) {
            explosion.Exploding = true;
            triggered = true;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Domino : MonoBehaviour {
    public float FollowDistance = 1f;
    public bool StartSleeping = true;
    public Vector3 FollowOffset= new Vector3(0, 1, 0);
    private Rigidbody _body;
    private Rigidbody Body {
        get {
            if (!_body)
                _body = GetComponent<Rigidbody>();
            return _body;
        }
    }

    private bool activated = true;

    private void Awake() {
        if(StartSleeping)
            Body.Sleep();
    }

    private void Start() {
        StartCoroutine(DelayedStart());
    }

    IEnumerator DelayedStart() {
        yield return new WaitForSeconds(1f);
        activated = false;
    }

    private void FixedUpdate() {
        if(Body.velocity.magnitude > 2)
            Trigger();
    }
    
    private void OnCollisionEnter(Collision col) {
        Trigger();
    }

    private void Trigger() {
        if (!activated)
        {
            SmoothCameraFollow.Target = transform;
            SmoothCameraFollow.Distance = FollowDistance;
            SmoothCameraFollow.Offset = FollowOffset;
            activated = true;
        }
    }
}

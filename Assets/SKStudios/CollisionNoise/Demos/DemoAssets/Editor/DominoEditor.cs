﻿using UnityEditor;
using UnityEngine;

namespace SKStudios.CollisionNoise
{
    [CustomEditor(typeof(Domino))]
    [CanEditMultipleObjects]
    public class DominoEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            Undo.RecordObject(this, "Domino Settle");
            if (GUILayout.Button("Settle Domino"))
            {
                foreach (GameObject d in Selection.objects) {
                    Domino targetDomino;
                    if (!(targetDomino = d.GetComponent<Domino>())) continue;
                    RaycastHit hit = new RaycastHit();
                    Ray ray = new Ray(targetDomino.transform.position, Vector3.down);
                    if (Physics.Raycast(ray, out hit, 10))
                    {
                        targetDomino.transform.position = hit.point + new Vector3(0, targetDomino.GetComponent<Renderer>().bounds.extents.y, 0);
                    }
                }
            }
            base.OnInspectorGUI();
        }
    }
}

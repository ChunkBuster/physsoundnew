﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothCameraFollow : MonoBehaviour {

    public static Transform Target;
    public static float Distance = 2f;
    public static Vector3 Offset;
    public Transform initialTarget;

    void Start() {
        Target = initialTarget;
    }
    
	// Update is called once per frame
	void Update () {
	    Vector3 targetPos = Target.position + Offset;
        transform.position = Vector3.Lerp(transform.position, ((transform.position - targetPos).normalized * Distance) + targetPos, Time.deltaTime);
	    transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(Target.position - transform.position, Vector3.up), Time.deltaTime);
    }
}

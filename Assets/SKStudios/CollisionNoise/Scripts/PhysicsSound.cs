﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKStudios.CollisionNoise {
    [RequireComponent(typeof(Collider))]
    public class PhysicsSound : MonoBehaviour {
        public const float MinSlideMagnitude = 0.01f;
        /*
        public AudioClip ImpactSound;
        public AudioClip DragSound;
        public float hardness = 0.5f;*/
        private ContactPoint[] _contactPoints;
        private Rigidbody _body;
        private Rigidbody Body {
            get {
                if (!_body)
                    _body = Col.attachedRigidbody;
                return _body;
            }
        }

        private Collider _col;
        private Collider Col {
            get {
                if (!_col)
                    _col = GetComponent<Collider>();
                return _col;
            }
        }

        private AudioSource _audioSource;
        private AudioSource AudioSource {
            get {
                if (!_audioSource) {
                    _audioSource = GetComponent<AudioSource>();
                    if (!_audioSource) {
                        _audioSource = gameObject.AddComponent<AudioSource>();
                        _audioSource.maxDistance = 100;
                    }        
                }
                return _audioSource;
            }
        }

        private Vector3 _lastVel = Vector3.zero;
        private Vector3 _lastAngVel = Vector3.zero;
        private void FixedUpdate() {
            Vector3 velocity = Body.velocity;
            Vector3 angVel = Body.angularVelocity;  
            _lastVel = velocity;
            _lastAngVel = angVel;
        }

        private void Start() {
            //NoiseDispatcher.AddPhysicsMaterialData(Col.sharedMaterial, ImpactSound, DragSound, hardness);
        }

        private void OnCollisionEnter(Collision col) {
            PNDispatch.Collide(Col, col);
        }

        private void OnCollisionStay(Collision col) {
            
            //Debug.Log(col.relativeVelocity.magnitude * Time.fixedDeltaTime);
            if (col.relativeVelocity.magnitude * Time.fixedDeltaTime < MinSlideMagnitude)
                PNDispatch.StopSlide(Col, col);
            else
                PNDispatch.Slide(Col, col);
        }

        private void OnCollisionExit(Collision col)
        {
            PNDispatch.StopSlide(Col, col);
        }
    }

}

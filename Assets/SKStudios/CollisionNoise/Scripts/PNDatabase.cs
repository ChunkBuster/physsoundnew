﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using SKStudios.CollisionNoise;
using UnityEngine;

namespace SKStudios.CollisionNoise {
    public class PNDatabase {
        #region const
        private static readonly string[] ResourceDirs = {
            "MaterialAudio"
        };
        private static readonly string[] DefaultData = {
            "PhysicMats/GenericMat",
            "AudioClips/GenericHit",
            "AudioClips/GenericSlide"
        };
        #endregion

        private static Dictionary<PhysicMaterial, MaterialAudioData> _clipData;

        /// <summary>
        /// Dictionary containing physics material data and their corresponding audio clips
        /// </summary>
        private static Dictionary<PhysicMaterial, MaterialAudioData> ClipData {
            get {
                if (_clipData == null) {
                    LoadDict();
                }
                return _clipData;
            }
        }

        private static void LoadDict() {
            _clipData = new Dictionary<PhysicMaterial, MaterialAudioData>();
            MaterialAudioData[] data = Resources.LoadAll<MaterialAudioData>(ResourceDirs[0]);
            foreach (MaterialAudioData d in data) {
                if (!_clipData.ContainsKey(d.Material))
                    _clipData.Add(d.Material, d);
            }
        }



        public static void RemovePhysicsMaterialData(PhysicMaterial mat) {
#if UNITY_EDITOR
            ClipData[mat].Delete();
#endif
            ClipData.Remove(mat);
        }

        public static void AddPhysicsMaterialData(PhysicMaterial mat) {
            MaterialAudioData data;
            if (!ClipData.TryGetValue(mat, out data))
                ClipData.Add(mat, MaterialAudioData.CreateInstance(mat));
            else
                ClipData[mat] = data;
        }

        public static MaterialAudioData[] GetMaterials() {
            return ClipData.Values.ToArray();
        }

        public static MaterialAudioData GetMatData(PhysicMaterial mat) {
            if (mat == null)
                return DefaultAudio;
            MaterialAudioData loaded;
            if (!ClipData.TryGetValue(mat, out loaded)) {
                MaterialAudioData defaultAudio = DefaultAudio;
                loaded = MaterialAudioData.CreateInstance(mat);

                loaded.Impact.Clip = defaultAudio.Impact.Clip;
                loaded.Slide.Clip = defaultAudio.Slide.Clip;
                loaded.Hardness = defaultAudio.Hardness;
                ClipData.Add(mat, loaded);
            }
            return loaded;

        }

        private static MaterialAudioData _defaultAudio;

        private static MaterialAudioData DefaultAudio {
            get {
                if (_defaultAudio == null) {
                    _defaultAudio = MaterialAudioData.CreateInstance(
                        Resources.Load<PhysicMaterial>(DefaultData[0]));
                    _defaultAudio.Impact.Clip = Resources.Load<AudioClip>(DefaultData[1]);
                    _defaultAudio.Slide.Clip = Resources.Load<AudioClip>(DefaultData[2]);
                    _defaultAudio.Hardness = 0.5f;
                }
                return _defaultAudio;
            }
        }


    }

}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Eppy;
using SKStudios.Common.Utils;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using Random = UnityEngine.Random;
namespace SKStudios.CollisionNoise
{
    public enum CollisionType
    {
        Impact,
        SlideStart,
        Slide
    }
    public class PNDispatch : MonoBehaviour
    {
        #region const
        /// <summary>
        /// Random variance in pitch of impacts
        /// </summary>
        public const float PitchVariance = 0.1f;
        /// <summary>
        /// Minimum pitch of sliding objects
        /// </summary>
        public const float MinSlidePitch = 0.5f;


        /// <summary>
        /// Scalar that all impact volume output is multiplied by
        /// </summary>
        public const float ImpactVolumeScalar = 3f;

        /// <summary>
        /// Scalar that all slide volume output is multiplied by
        /// </summary>
        public const float SlideVolumeScalar = 0.05f;

        /// <summary>
        /// Scalar that all impact pitch output is multiplied by
        /// </summary>
        public const float ImpactPitchScalar = 1f;

        /// <summary>
        /// Scalar that all slide pitch output is multiplied by
        /// </summary>
        public const float SlidePitchScalar = 1f;

        public const int InitAudioSources = 30;
        public const int MaxAudioSources = 30;

        #endregion

        #region coroutineControl
        private static readonly WaitForEndOfFrame WaitForEndOfFrame = new WaitForEndOfFrame();
        private static readonly WaitForFixedUpdate WaitForFixedUpdate = new WaitForFixedUpdate();
        private static readonly WaitForSeconds DelayedPoll = new WaitForSeconds(0.2f);
        private static readonly float SlideTimeoutLength = 0.3f;
        #endregion

        private static PNDispatch _instance;
        public static PNDispatch Instance {
            get {
                if (!_instance)
                {
                    GameObject instanceObject = new GameObject();
                    instanceObject.hideFlags = HideFlags.HideAndDontSave;
                    _instance = instanceObject.AddComponent<PNDispatch>();
                }
                return _instance;
            }
        }

        private static readonly Dictionary<Tuple<Collider, Collider>, Collision> _touchingColliders 
            = new Dictionary<Tuple<Collider, Collider>, Collision>();
        private static readonly Dictionary<Tuple<Collider, Collider>, Vector3[]> _currentContactPoints
            = new Dictionary<Tuple<Collider, Collider>, Vector3[]>();
        private static readonly Dictionary<Tuple<Collider, Collider>, Vector3[]> _lastContactPoints 
            = new Dictionary<Tuple<Collider, Collider>, Vector3[]>();

        private static NoisePool _pool;
        private static NoisePool Pool {
            get {
                if (_pool == null)
                {
                    _pool = new NoisePool(InitAudioSources, MaxAudioSources);
                }
                return _pool;
            }
        }

        public static void Collide(Collider original, Collision col)
        {
            Tuple<Collider, Collider> key = new Tuple<Collider, Collider>(original, col.collider);
            //Break if object is sliding
            if (original is SphereCollider
                && _touchingColliders.ContainsKey(key)) return;
            MaterialAudioData sourceData = PNDatabase.GetMatData(original.sharedMaterial);
            MaterialAudioData targetData = PNDatabase.GetMatData(col.collider.sharedMaterial);
            float scalar = AttenuationUtils.ImpactScalarNormalized(col);

            
            Collider activeCollider = sourceData.Hardness < targetData.Hardness ? original : col.collider;
            MaterialAudioData activeData = sourceData.Hardness < targetData.Hardness ? sourceData : targetData;
            Instance.StartCoroutine(Instance.PlayImpact(key, col, activeData, scalar, activeCollider));

            Vector3[] contactPoints = new Vector3[col.contacts.Length];
            for (int i = 0; i < col.contacts.Length; i++)
            {
                ContactPoint c = col.contacts[i];
                _currentContactPoints[key] = contactPoints;
                _lastContactPoints[key] = contactPoints;
            }
        }

        public static void Slide(Collider original, Collision col)
        {
            MaterialAudioData sourceData = PNDatabase.GetMatData(original.sharedMaterial);
            MaterialAudioData targetData = PNDatabase.GetMatData(col.collider.sharedMaterial);

            Tuple<Collider, Collider> key = new Tuple<Collider, Collider>(original, col.collider);

            
            Vector3[] contactPoints = new Vector3[col.contacts.Length];
            for (int i = 0; i < col.contacts.Length; i++)
            {
                ContactPoint c = col.contacts[i];
                contactPoints[i] = c.point;
            }
           
            
            if (_touchingColliders.ContainsKey(key))
            {
                if (_touchingColliders[key].contacts.Length < col.contacts.Length)
                    Collide(original, col);
                _touchingColliders[key] = col;
                _currentContactPoints[key] = contactPoints;
            }
            else
            {
                _touchingColliders.Add(key, col);
                _currentContactPoints[key] = contactPoints;
                _lastContactPoints[key] = contactPoints;
                {
                    Collider activeCollider = sourceData.Hardness < targetData.Hardness ? original : col.collider;
                    Instance.StartCoroutine(Instance.PlaySlide(key, activeCollider));
                }
            }
        }

        public static void StopSlide(Collider original, Collision col)
        {
            Tuple<Collider, Collider> key = new Tuple<Collider, Collider>(original, col.collider);
            _touchingColliders.Remove(key);
            _currentContactPoints.Remove(key);
            _lastContactPoints.Remove(key);
        }

        private IEnumerator PlaySlide(Tuple<Collider, Collider> key, Collider active)
        {
            float deltaMassScalarNorm = AttenuationUtils.MassScalarNormalized(key.Item1, key.Item2);
            float deltaMassScalar = AttenuationUtils.MassScalar(key.Item1, key.Item2);
            MaterialAudioData data = PNDatabase.GetMatData(active.sharedMaterial);
            AudioClip clip = data.Slide.Clip;
            //If there is no clip, exit
            if (!clip)
                yield break;
            AudioSource source = Pool.GetSource();
            //If there is no source, exit
            if (!source)
                yield break;
            source.minDistance = (active.bounds.size.magnitude) / 2f;
            source.loop = true;
            source.clip = clip;
            source.time = Random.Range(0, source.clip.length);
           
           

            float fadeintime = 0.1f;
            float startTime = Time.time;
            Collision col = null;
            Vector3 center = Vector3.zero;

            Action calcCenter = () => {
                col = _touchingColliders[key];
                center = Vector3.zero;
                foreach (ContactPoint p in col.contacts)
                    center += p.point;
                center /= col.contacts.Length;
            };

            calcCenter();
            source.transform.position = center;

            bool timeout = false;
            bool timeoutStarted = false;
            source.transform.SetParent(active.transform);
#if SKS_DEBUG_SOUND
            Instance.StartCoroutine(SoundDebug.DrawDebugSource(source.transform.position, 5f,
                CollisionType.SlideStart));
#endif
            bool initialized = false;
            float timeoutStart = 0;
            while (true)
            {
                while (_touchingColliders.ContainsKey(key))
                {
                    
                    timeout = false;
                    timeoutStarted = false;
                    calcCenter();
                    //source.transform.position = Vector3.Lerp(source.transform.position, center, Time.deltaTime * 10);
                    float fadeinScalar = Mathf.Lerp(0, 1, (Time.time - (startTime + fadeintime)) / fadeintime);
                    Vector3[] lastContacts = _currentContactPoints[key];
                    yield return new WaitForFixedUpdate();
                    Vector3[] currentContacts;
                    if (!_currentContactPoints.TryGetValue(key, out currentContacts)
                        /* || !_lastContactPoints.ContainsKey(key)*/ || currentContacts.Length != lastContacts.Length)
                        continue;
                    float shearScalar = AttenuationUtils.ShearScalarNormalized(key.Item1, key.Item2, col,
                        _currentContactPoints[key],
                        lastContacts);
                    float massScalar = (1 - deltaMassScalarNorm);
                    float targetPitch = (shearScalar * data.Slide.Pitch * SlidePitchScalar * (massScalar > MinSlidePitch ? massScalar : MinSlidePitch));
                    if (!initialized) {
                        initialized = true;
                        source.Play();
                        source.pitch = targetPitch;
                    }
                    else {
                        source.pitch = Mathf.Lerp(source.pitch, targetPitch, Time.deltaTime * 2f);
                    }

                    source.volume = shearScalar * fadeinScalar * data.Slide.Volume * deltaMassScalar * SlideVolumeScalar;
#if SKS_DEBUG_SOUND
                    Instance.StartCoroutine(SoundDebug.DrawDebugSource(source.transform.position, 2f,
                        CollisionType.SlideStart));
#endif
                    //source.volume = key.Item1.attachedRigidbody.velocity.magnitude;
                }
                if (timeout)
                    break;
                initialized = false;
                
                if (!timeoutStarted)
                    timeoutStart = Time.time;

                timeoutStarted = true;
                if (Time.time < timeoutStart + SlideTimeoutLength)
                {
                    source.volume = Mathf.Lerp(source.volume, 0, Time.deltaTime * 2f);
                    yield return new WaitForFixedUpdate();
                }
                else
                {
                    timeout = true;
                }

            }
            source.Stop();
            Pool.RemoveSource(source);
        }

        private IEnumerator PlayImpact(Tuple<Collider, Collider> key, Collision col, MaterialAudioData data,
            float scalar, Collider active)
        {
            AudioClip clip = data.Impact.Clip;
            if (clip == null)
                yield break;
            AudioSource source = Pool.GetSource();
            if (!source)
                yield break;
            source.minDistance = (active.bounds.size.magnitude) / 2f;
            source.transform.position = col.contacts[0].point;
            source.clip = clip;

            source.spatialBlend = 1f;
            source.volume = data.Hardness * AttenuationUtils.MassNormalized(active) * data.Impact.Volume * scalar * ImpactVolumeScalar;

            source.pitch = ((1 - AttenuationUtils.MassScalarNormalized(active, null)) * data.Impact.Pitch * ImpactPitchScalar) + 
                (Random.value * PitchVariance);
#if SKS_DEBUG_SOUND
                Instance.StartCoroutine(SoundDebug.DrawDebugSource(source.transform.position, 5f,
                    CollisionType.Impact));
#endif
            source.Play();


            yield return new WaitForSeconds(source.clip.length / source.pitch);
            Pool.RemoveSource(source);
        }

        private static IEnumerator UpdateContactPoints() {
            while (true) {
                foreach (Tuple<Collider, Collider> c in _currentContactPoints.Keys)
                    _lastContactPoints[c] = _currentContactPoints[c];
                yield return WaitForFixedUpdate;
            }
        }


#if UNITY_EDITOR
        /*
        public void OnGUI()
        {
            Handles.color = Color.blue;
            foreach (AudioSource a in Pool)
            {
                Handles.Label(a.transform.position, a.volume.ToString());
            }
        }*/
#endif

        private void Start() {
            Instance.StartCoroutine(UpdateContactPoints());
#if SKS_DEBUG_SOUND
            PrimitiveHelper.GetPrimitiveMesh(PrimitiveType.Cube);
#endif
        }
#if SKS_DEBUG_SOUND
        internal class SoundDebug {
            private static readonly Material _impactMat, _slideStartMat, _slideMat;
            static SoundDebug() {
                Shader shader = Shader.Find("Standard");
                _impactMat = new Material(shader);
                _impactMat.color = Color.red;
                _slideStartMat = new Material(shader);
                _slideStartMat.color = Color.magenta;
                _slideMat = new Material(shader);
                _slideMat.color = Color.blue;
            }

            public static IEnumerator DrawDebugSource(Vector3 position, float time, CollisionType type)
            {
                float startTime = Time.time;
                Material mat;
                switch (type) {
                    default:
                    case (CollisionType.Impact):
                        mat = _impactMat;
                        break;
                    case (CollisionType.SlideStart):
                        mat = _slideStartMat;
                        break;
                    case (CollisionType.Slide):
                        mat = _slideMat;
                        break;  
                }

                while (Time.time < startTime + time || time < 0f)
                {
                    Matrix4x4 matrix = Matrix4x4.TRS(position, Quaternion.identity, new Vector3(0.1f, 0.1f, 0.1f));
                    mat.SetPass(1);
                    Graphics.DrawMeshNow(PrimitiveHelper.GetPrimitiveMesh(PrimitiveType.Cube), matrix);
                    yield return new WaitForEndOfFrame();
                }
            }
        }
#endif
    }
}

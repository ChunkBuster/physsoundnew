﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace SKStudios.CollisionNoise {
    public static class AttenuationUtils {
        #region const
        public const float MaxCollisionMagnitude = 1f;
        public const float MaxMassScalar = 20f;
        public const float MaxSlideScalar = 0.05f;
        public const float MinImpactAmount = 0.0002f;
        public const float MinShearAmount = 0.001f;
        public const float MassShearContribution = 0.7f;
        #endregion

        /// <summary>
        /// Returns a normalized scalar for shear calculation
        /// Factors in: Slide velocity
        /// </summary>
        /// <param name="col1">Original collider</param>
        /// <param name="col">Collision data</param>
        /// <returns>normalized scalar</returns
        public static float ShearScalarNormalized(Collider col1, Collider col2, Collision col, 
            Vector3[] lastColPoints, Vector3[] currColPoints)
        {
            Vector3 relativeSlide = Vector3.zero;
            int smallestIdex = Mathf.Min(lastColPoints.Length, currColPoints.Length);
            Vector3 avgInitPos = Vector3.zero;
            Vector3 avgDestPos = Vector3.zero;
            for (int i = 0; i < smallestIdex; i++) {
                avgInitPos += lastColPoints[i];
                avgDestPos += currColPoints[i];
            }
            avgInitPos /= smallestIdex;
            avgDestPos /= smallestIdex;
            relativeSlide = avgDestPos - avgInitPos;
            //return (Mathf.Min(relativeSlide.magnitude, MaxSlideScalar) / MaxSlideScalar); 
            //Debug.Log(relativeSlide.magnitude, col1);
            return Mathf.Log((relativeSlide.magnitude * 10f) + 1.5f) / 2f;
        }

        /// <summary>
        /// Returns a normalized scalar based on the impact velocity of the collision
        /// Factors in: Impact speed
        /// </summary>
        /// <param name="col">Collision</param>
        /// <returns>The normalized scalar value.</returns>
        public static float ImpactScalarNormalized(Collision col)
        {
            float magnitude = Mathf.Max(col.impulse.magnitude * Time.fixedDeltaTime);
            return (Mathf.Min(magnitude, MaxCollisionMagnitude) / MaxCollisionMagnitude);
        }

        /// <summary>
        /// Returns a scalar based on the angular velocities of the two colliders given.
        /// Factors in: Angular Velocity
        /// </summary>
        /// <param name="col1">Collider 1</param>
        /// <param name="col2">Collider 2</param>
        /// <returns>The non-normalized scalar value.</returns>
        public static float RelativeAngVel(Collider col1, Collider col2)
        {
            Vector3 relativeAngVel = Vector3.zero;
            if (!col1.attachedRigidbody)
            {
                if (col2.attachedRigidbody)
                    relativeAngVel = col2.attachedRigidbody.angularVelocity;
            }
            else if (!col2.attachedRigidbody)
            {
                if (col1.attachedRigidbody)
                    relativeAngVel = col1.attachedRigidbody.angularVelocity;
            }
            else
            {
                relativeAngVel = col1.attachedRigidbody.angularVelocity - col2.attachedRigidbody.angularVelocity;
            }

            return relativeAngVel.magnitude;
        }

        /// <summary>
        /// Returns a scalar based on the masses of the two colliders given.
        /// Factors in: Mass
        /// </summary>
        /// <param name="col">Collider 1</param>
        /// <param name="col2">Collider 2</param>
        /// <returns>The normalized scalar value.</returns>
        public static float MassScalarNormalized(Collider col, Collider col2) {
            return Mathf.Min(MassScalar(col, col2), MaxMassScalar) / MaxMassScalar;
        }

        /// <summary>
        /// Returns a scalar based on the masses of the two colliders given.
        /// Factors in: Mass
        /// </summary>
        /// <param name="col">Collider 1</param>
        /// <param name="col2">Collider 2</param>
        /// <returns>The non-normalized scalar value.</returns>
        public static float MassScalar(Collider col, Collider col2)
        {
            float massScalar = 1f;
            if (col && col.attachedRigidbody)
                massScalar *= col.attachedRigidbody.mass;
            if (col2 && col2.attachedRigidbody)
                massScalar *= col2.attachedRigidbody.mass;
            return massScalar;
        }

        /// <summary>
        /// Returns a normalized value given a mass
        /// Factors in: Mass
        /// </summary>
        /// <param name="col">Collider 1</param>
        /// <returns>The normalized scalar value.</returns>
        public static float MassNormalized(Collider col)
        {
            float massScalar = 1f;
            if (col.attachedRigidbody)
                massScalar *= col.attachedRigidbody.mass;
            return massScalar;
        }
    }
}

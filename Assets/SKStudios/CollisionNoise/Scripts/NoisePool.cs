﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SKStudios.CollisionNoise {
    public class NoisePool : IEnumerable {
        private int _poolSize;
        private int _maxSize;
        private readonly List<AudioSource> _allSources;
        private readonly Queue<AudioSource> _availableSources;
        private readonly RemovableQueue<AudioSource> _usedSources;

        public NoisePool(int poolSize = 10, int maxSize = 10) {
            if (poolSize < 1)
                throw new ArgumentException("Minimum pool size for NoiseSpool is 1.");
            _availableSources = new Queue<AudioSource>();
            _usedSources = new RemovableQueue<AudioSource>();
            _allSources = new List<AudioSource>();
           _poolSize = poolSize;
            _maxSize = maxSize;
            for (int i = 0; i < poolSize; i++)
                _availableSources.Enqueue(NewSource());
        }


        public IEnumerator GetEnumerator() {
            return _allSources.GetEnumerator();
        }


        private AudioSource NewSource() {
            GameObject obj = new GameObject();
#if UNITY_EDITOR
#if !SKS_DEV
            obj.hideFlags = HideFlags.HideAndDontSave;
#else
            obj.AddComponent<SelfDestruct>();
#endif
#endif
            AudioSource source = obj.AddComponent<AudioSource>();
            _allSources.Add(source);
            return source;
        }

        public AudioSource GetSource() {
            AudioSource source;
            if (_availableSources.Count > 0) {
                source = _availableSources.Dequeue();
            }
            else {
                if (true) {
                    source = NewSource();
                }
                else {
                    source = null;
                }
            }
            source.mute = true;
            source.loop = false;
            source.spatialBlend = 1;
            source.time = 0;
            source.minDistance = 0;
            source.pitch = 1;
            source.transform.parent = null;
            source.mute = false;
            return source;
        }

        public void RemoveSource(AudioSource source)
        {
            source.Stop();
            source.mute = true;
            source.clip = null;
            _availableSources.Enqueue(source);
            _usedSources.Remove(source);
        }


        public void OnApplicationQuit() {
            foreach (AudioSource a in _availableSources) {
                if(a)
                    GameObject.DestroyImmediate(a, true);
            }
                
            foreach (AudioSource a in _usedSources) {
                if(a)
                    GameObject.DestroyImmediate(a, true);
            }
                
        }

        internal class RemovableQueue<T> : IEnumerable
        {
            LinkedList<T> list = new LinkedList<T>();
            public void Enqueue(T t)
            {
                list.AddLast(t);
            }

            public T Dequeue()
            {
                var result = list.First.Value;
                list.RemoveFirst();
                return result;
            }

            public T Peek()
            {
                return list.First.Value;
            }

            public bool Remove(T t)
            {
                return list.Remove(t);
            }

            public IEnumerator GetEnumerator()
            {
                return list.GetEnumerator();
            }

            public int Count { get { return list.Count; } }
        }
    }
}
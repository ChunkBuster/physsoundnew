﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SKStudios.CollisionNoise
{

    [ExecuteInEditMode]
    [InitializeOnLoad]
    public class SelfDestruct : MonoBehaviour
    {
        private bool createdState;

        void Start()
        {
            createdState = Application.isPlaying;
        }
        void Update()
        {
            if (createdState != Application.isPlaying)
                DestroyImmediate(this, true);
        }
    }

}
#endif
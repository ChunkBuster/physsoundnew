﻿using System;
using UnityEngine;
using System.Collections;
using SKStudios.Common.Utils;
#if UNITY_EDITOR
using UnityEditor;
#endif
namespace SKStudios.CollisionNoise {
    [System.Serializable]
    public class AudioData
    {
        [SerializeField] public String Name;

        [SerializeField] private String _clipGuid;
        private String ClipGuid {
            get {
                if (String.IsNullOrEmpty(_clipGuid))
                    ReassessPaths();
                return _clipGuid;
            }
        }

        [SerializeField] private String _clipResourcePath;
        private String ClipResourcePath {
            get {
                if (String.IsNullOrEmpty(_clipResourcePath))
                    ReassessPaths();
                return _clipResourcePath;
            }
        }

        [NonSerialized] private AudioClip _clip;
        public AudioClip Clip {
            get {
                if (_clip == null)
                {
#if UNITY_EDITOR
                    if (!Application.isPlaying)
                    {
                        _clip = AssetDatabase.LoadAssetAtPath<AudioClip>(AssetDatabase.GUIDToAssetPath(ClipGuid));
                    }
                    else
                    {
#endif
                        //_clip = Resources.Load<AudioClip>(ClipResourcePath); 
                        _clip = LazyAssetLoader.GetAsset<AudioClip>(ClipResourcePath);
#if UNITY_EDITOR
                    }
#endif
                }
                return _clip;
            }
            set {
                _clip = value;
                ReassessPaths();
            }
        }

        private void ReassessPaths() {
#if UNITY_EDITOR
            _clipResourcePath = AssetDatabase.GetAssetPath(_clip);
            _clipGuid = AssetDatabase.AssetPathToGUID(_clipResourcePath);
#endif
            if (!String.IsNullOrEmpty(_clipResourcePath))
            {
                String searchString = "Resources/";
                int startIndex = _clipResourcePath.IndexOf(searchString) + searchString.Length;
                int endIndex = _clipResourcePath.LastIndexOf('.');
                _clipResourcePath = _clipResourcePath.Substring(
                    startIndex,
                    endIndex - startIndex
                );
            }
        }

        public float Volume = 1f;
        public float Pitch = 1f;
        public int Priority = 0;

        public AudioData(String name)
        {
            Name = name;
        }
    }

    [CreateAssetMenu(fileName = "MaterialAudioData", menuName = "ScriptableObject/", order = 1)]
    public class MaterialAudioData : ScriptableObject {
        [SerializeField] public AudioData Impact;
        [SerializeField] public AudioData HardImpact;
        [SerializeField] public AudioData Slide;
        [SerializeField] public AudioData Roll;

        private string Path {
            get {
                return ("Assets/SKStudios/CollisionNoise/Resources/MaterialAudio/" + name + ".asset");
            }
        }

        public PhysicMaterial Material;

        public float Hardness;

        public static MaterialAudioData CreateInstance(PhysicMaterial material) {
            MaterialAudioData newData = ScriptableObject.CreateInstance<MaterialAudioData>();
            newData.Material = material;
            newData.name = material.name;
            newData.Impact = new AudioData("Impact");
            newData.HardImpact = new AudioData("Hard Impact");
            newData.Slide = new AudioData("Slide");
            newData.Roll = new AudioData("Roll");
            newData.Save();
            return newData;
        }

        public void Save() {
#if UNITY_EDITOR
            Undo.RecordObject(this, "Updated Material Audio Data for " + name);
            EditorUtility.SetDirty(this);
            MaterialAudioData data = AssetDatabase.LoadAssetAtPath<MaterialAudioData>(this.Path);
            if(!data){
                AssetDatabase.CreateAsset(this, Path);
            }

            AssetDatabase.SaveAssets();
#endif
        }

        public void Delete() {
#if UNITY_EDITOR
            Undo.RecordObject(this, "Deleted Material Audio Data for " + name);
            AssetDatabase.DeleteAsset(Path);
            AssetDatabase.Refresh();
#endif
        }

    }
}
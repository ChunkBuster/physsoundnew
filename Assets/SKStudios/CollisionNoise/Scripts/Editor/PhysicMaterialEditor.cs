﻿using UnityEditor;
using UnityEngine;

namespace SKStudios.CollisionNoise {
    [CustomEditor(typeof(PhysicMaterial))]
    public class PhysicMaterialEditor : Editor {
        public override void OnInspectorGUI() {
            MaterialAudioData data = PNDatabase.GetMatData((PhysicMaterial) target);
            base.OnInspectorGUI();
            GUILayout.Space(10);
            GUILayout.Label("Material Audio Properties:");
            MaterialAudioEditor.DrawInspector(data);
        }
    }
}

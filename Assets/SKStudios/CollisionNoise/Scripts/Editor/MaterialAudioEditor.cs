﻿using System;
using UnityEngine;
using UnityEditor;

namespace SKStudios.CollisionNoise {
    public class MaterialAudioEditor : EditorWindow {
        private PhysicMaterial _newMat;
        private AudioClip _newClip;
        private AudioClip _newSlideClip;
        private float _newHardness;
        private Vector2 _scrollPos;
        private bool[] _foldoutValues;

        internal class Content {
            public static GUIContent NewItem = new GUIContent("New Entry:");
        }

        [MenuItem("Tools/SK Studios/PhysicsNoise")]
        public static void Open() {
            MaterialAudioEditor editor = GetWindow<MaterialAudioEditor>();
            editor.titleContent = new GUIContent("Physics Noises");
        }

        public void OnGUI() {
            MaterialAudioData[] mats = PNDatabase.GetMaterials();
            Array.Sort(mats, (x, y) => x.name.CompareTo(y.name));
            if (_foldoutValues == null) {
                _foldoutValues = new bool[mats.Length];
            }

            if (_foldoutValues.Length != mats.Length) {
                bool[] newFolds = new bool[mats.Length];
                for (int i = 0; i < Math.Min(newFolds.Length, _foldoutValues.Length); i++) {
                    newFolds[i] = _foldoutValues[i];
                }
                _foldoutValues = newFolds;

            }

            GUILayout.Label("Physics Material Sounds");
            _scrollPos = GUILayout.BeginScrollView(_scrollPos);
            for (var i = 0; i < mats.Length; i++) {
                var data = mats[i];
                if (data == null)
                    continue;

                EditorGUI.BeginChangeCheck();

                if (_foldoutValues[i])
                    GUILayout.BeginVertical((GUIStyle) "flow overlay box");
                else
                    GUILayout.BeginVertical();
                {

                    GUIContent name = new GUIContent(data.name);

                    GUILayout.BeginHorizontal();
                    GUILayout.BeginVertical();
                    if (_foldoutValues[i] = EditorGUILayout.Foldout(_foldoutValues[i], name)) {
                        PhysicMaterial mat =
                            (PhysicMaterial) EditorGUILayout.ObjectField(data.Material, typeof(PhysicMaterial), false);
                        DrawInspector(data);
                    }
                }
                GUILayout.EndVertical();
                if (GUILayout.Button("-", GUILayout.MaxWidth(25))) {
                    PNDatabase.RemovePhysicsMaterialData(data.Material);
                }
                GUILayout.EndHorizontal();
                GUILayout.EndVertical();
            }
            GUILayout.EndScrollView();
            GUILayout.Space(15);

            GUILayout.BeginVertical((GUIStyle) "flow overlay box");
            {
                EditorGUILayout.BeginHorizontal();
                {
                    EditorGUILayout.LabelField(Content.NewItem,
                        GUILayout.MaxWidth(GUI.skin.label.CalcSize(Content.NewItem).x));
                    GUILayout.FlexibleSpace();
                    if (GUILayout.Button("+", GUILayout.MaxWidth(25))) {
                        if (!_newMat || !_newClip || !_newSlideClip) return;
                        Undo.RecordObject(PNDispatch.Instance, "New Physics Noise Data Added");
                        PNDatabase.AddPhysicsMaterialData(_newMat);
                    }
                }
                EditorGUILayout.EndHorizontal();
                _newMat = (PhysicMaterial) EditorGUILayout.ObjectField(_newMat, typeof(PhysicMaterial), false);
                _newClip = (AudioClip) EditorGUILayout.ObjectField(_newClip, typeof(AudioClip), false);
                _newSlideClip = (AudioClip) EditorGUILayout.ObjectField(_newSlideClip, typeof(AudioClip), false);
                _newHardness = EditorGUILayout.FloatField("Hardness", Mathf.Clamp(_newHardness, 0f, 1f));

            }
            GUILayout.EndVertical();
        }

        //Todo: Move this to an editor class
        public static void DrawInspector(MaterialAudioData data) {
            EditorGUI.BeginChangeCheck();
            {
                DrawClipEditor(ref data.Impact);
                DrawClipEditor(ref data.HardImpact, data.Impact);
                DrawClipEditor(ref data.Slide);
                DrawClipEditor(ref data.Roll, data.Slide);
            }
            data.Hardness = EditorGUILayout.FloatField("Hardness", Mathf.Clamp(data.Hardness, 0f, 1f));
            if (EditorGUI.EndChangeCheck()) {
                PNDatabase.AddPhysicsMaterialData(data.Material);
                data.Save();
            }

        }

        static void DrawClipEditor(ref AudioData data, AudioData ovr = null) {
            //todo: Move all of the following to an internal class inside the aforementioned editor class
            Color lineColor = EditorGUIUtility.isProSkin
                ? new Color(0.157f, 0.157f, 0.157f)
                : new Color(0.5f, 0.5f, 0.5f);
            GUIContent volumeContent = new GUIContent("Volume");
            GUIContent pitchContent = new GUIContent("Pitch");
            GUIContent priorityContent = new GUIContent("Priority");
            GUILayoutOption width = GUILayout.MaxWidth(GUI.skin.label.CalcSize(priorityContent).x);
            Vector2 indent = new Vector3(0, 0);
            //End todo

            GUILayout.BeginHorizontal();
            {
                data.Clip =
                    (AudioClip) EditorGUILayout.ObjectField(data.Name, data.Clip, typeof(AudioClip),
                        false);
                if (data.Clip == null) {
                    if (ovr != null) {
                        GUILayout.Label("Unassigned, Overridden by " + ovr.Name);
                    }
                    GUILayout.EndHorizontal();
                    return;
                }
            }
            GUILayout.EndHorizontal();

            Handles.BeginGUI();
            Color handleColor = Handles.color;
            Handles.color = lineColor;

            EditorGUI.indentLevel++;
            Rect noiseRect = GUILayoutUtility.GetLastRect();
            Rect volumeRect = GUILayoutUtility.GetRect(volumeContent, GUI.skin.label);
            data.Volume = EditorGUI.FloatField(volumeRect, volumeContent, data.Volume);
            Vector2 lineBegin = volumeRect.position + new Vector2(0, (volumeRect.height / 2));
            Handles.DrawLine(
                lineBegin,
                lineBegin + new Vector2(EditorGUI.indentLevel * 10f, 0));
            Rect pitchRect = GUILayoutUtility.GetRect(pitchContent, GUI.skin.label);
            lineBegin = pitchRect.position + new Vector2(0, (pitchRect.height / 2));
            Handles.DrawLine(
                lineBegin,
                lineBegin + new Vector2(EditorGUI.indentLevel * 10f, 0));
            data.Pitch = EditorGUI.FloatField(pitchRect, pitchContent, data.Pitch);

            Rect priorityRect = GUILayoutUtility.GetRect(priorityContent, GUI.skin.label);
            lineBegin = priorityRect.position + new Vector2(0, (priorityRect.height / 2));
            Handles.DrawLine(
                lineBegin,
                lineBegin + new Vector2(EditorGUI.indentLevel * 10f, 0));
            data.Priority = EditorGUI.IntField(priorityRect, priorityContent, data.Priority);

            Rect lastRect = GUILayoutUtility.GetLastRect();
            Handles.DrawLine(
                noiseRect.position + new Vector2(0, (noiseRect.height / 2)),
                lastRect.position + indent + new Vector2(0, (lastRect.height / 2)));
            EditorGUI.indentLevel--;
            Handles.color = handleColor;

            Handles.EndGUI();
        }
    }
}
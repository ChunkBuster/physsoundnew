﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace SKStudios.CollisionNoise {
    [CustomEditor(typeof(PhysicsSound))]
    public class NewBehaviourScript : Editor {
        private Collider _col;
        private Collider Col {
            get {
                if (!_col)
                    _col = ((PhysicsSound) target).gameObject.GetComponent<Collider>();
                return _col;
            }
        }
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            if (!Col)
                return;

            MaterialAudioData data = PNDatabase.GetMatData(Col.sharedMaterial);
            GUILayout.Space(10);
            GUILayout.Label("Material Audio Properties for the PhysicMaterial on this collider:");
            MaterialAudioEditor.DrawInspector(data);
        }
    }
}

